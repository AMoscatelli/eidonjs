<?php
require '../php/config.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="cache-control" content="no-cache" />
	<!--CSS con customizzazioni-->
	<!--<link rel="stylesheet" href="../cssNew/custom_calendar.css">-->
	
	<!--Link per le icone del context menu-->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<!--JS JQuery e CSS Bootstrap material design-->
	<script src="../jsNew/jquery.tools.min.1.2.7.js"></script>
	<link rel="stylesheet" href="../cssNew/bootstrap-material-design.min_4.0.0.css">
	<script src="../jsNew/jquery-2.2.4.min.js"></script>
	
	<script src="../jsNew/popper_1.12.6.js"></script>
	<script src="../jsNew/bootstrap-material-design_4.0.0.js"></script>
	<link rel="stylesheet" href="../cssNew/bootstrap-material-datetimepicker_2.7.1.css"/>
    <script src="../agenda/fullcalendar/lib/moment.min.js"></script>
	<script src="../jsNew/jquery.jqgrid.min_4.15.2.js"></script>
	<script src="../jsNew/bootstrap-material-datetimepicker.js"></script>
	
	<!--jqgrid-->
	<link rel="stylesheet" href="../cssNew/ui.jqgrid.min_4.15.2.css">
	<link rel="stylesheet" href="../cssNew/jquery-ui.min_1.11.4.css">
	
    <!--<link rel="stylesheet" href="../cssNew/font-awesome.min_4.7.0.css">-->
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--Context menu-->
	<script src="../jsNew/jquery.contextMenu_2.7.0.js"></script>
	<link rel='stylesheet' href='../cssNew/jquery.contextMenu_2.7.0.css'/>
	
	
	<script src="https://apis.google.com/js/api.js"></script>
	<link type="text/css" href="../cssNew/jqGridCustom.css" rel="stylesheet" />
	
	<!-- full calendar -->
	<link rel='stylesheet' href='../agenda/fullcalendar/fullcalendar.min.css' />
	<script src='../agenda/fullcalendar/fullcalendar.js'></script>
	<script src='../agenda/fullcalendar/gcal.js'></script>
	<script src='../agenda/fullcalendar/locale/it.js'></script>
	
	<!-- custom -->
	<script> 
	var googleAPI = "<?php echo $googleAPI; ?>"; 
	var client_id = "<?php echo $clientID;  ?>";
	</script>
	<script src='../jsNew/data.js'></script>
	<script src='../jsNew/cookie.js'></script>
	<script src='../jsNew/google.js'></script>
	<script src='../jsNew/json.js'></script>
	
	
	<script src='../jsNew/menu.js'></script>
	<script src='../jsNew/scuola.js'></script>
	<script src='../jsNew/info.js'></script>
	<link rel="stylesheet" href="../cssNew/custom_calendar.css">
	<script>
	$('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
      });
	  
	window.onload = function(e){ 
		checkCookie();
					
		if (username[0]=="1"){
					$('#TerapistaAppuntamento_mod').prop( "disabled", false );
		}

	}

	</script>

</head>
<body>

<div id='calendar'></div>

<div class="modal fade" id="mostraEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="TitoloAppuntamento"></h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Nome Paziente:</label>
				<input type="text" class="form-control" id="NomeAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Data appuntamento:</label>
				<input type="text" class="form-control" id="DataAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora inizio:</label>
				<input type="text" class="form-control" id="OraStartAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora fine:</label>
				<input type="text" class="form-control" id="OraEndAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Terapista:</label>
				<input type="text" class="form-control" id="TerapistaAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Stanza:</label>
				<input type="text" class="form-control" id="StanzaAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Tariffa:</label>
				<input type="text" class="form-control" id="TariffaAppuntamento" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Note:</label>
				<input type="text" class="form-control" id="NoteAppuntamento" disabled></input>
			</div>
		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
        <!-- <button type="button" class="btn btn-primary" onclick="MostraModalModifica()">Modifica</button> -->
		
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mostraEventoDopoScuola" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="TitoloAppuntamentoDS"></h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
			
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Data appuntamento:</label>
				<input type="text" class="form-control" id="DataAppuntamentoDS" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora inizio:</label>
				<input type="text" class="form-control" id="OraStartAppuntamentoDS" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora fine:</label>
				<input type="text" class="form-control" id="OraEndAppuntamentoDS" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Note:</label>
				<input type="text" class="form-control" id="NoteAppuntamentoDS" disabled></input>
			</div>
		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mostraEventoGoogle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="TitoloAppuntamentoGoogle"></h6>&nbsp;&nbsp;<img src="images/google_icon2.png" width="10%">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
		
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Data appuntamento:</label>
				<input type="text" class="form-control" id="DataAppuntamentoGoogle" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora inizio:</label>
				<input type="text" class="form-control" id="OraStartAppuntamentoGoogle" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Ora fine:</label>
				<input type="text" class="form-control" id="OraEndAppuntamentoGoogle" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Note:</label>
				<input type="text" class="form-control" id="NoteAppuntamentoGoogle" disabled></input>
			</div>		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
		<button type="button" class="btn btn-primary" onclick="MostraModalModifica('google')">Sincronizza con IGEA</button>
		
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="sincronizzaGoogle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="overflow-y: scroll;  max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
		
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Titolo appuntamento:</label>
				<input type="text" class="form-control" id="TitoloAppuntamento_google"></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Nome Paziente:</label>
				<input type="text" class="form-control" id="NomeAppuntamento_google" onclick="MostraModalCercaPaziente('google')"></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Data appuntamento:</label>
				<input type="text" class="form-control" id="DataAppuntamento_google" onclick="modificaDataInizio_google()"></input>
				<input type="text" id="data_google" class="form-control" style="display:none" placeholder="Seleziona data appuntamento">
							<script>
							$('#data_google').bootstrapMaterialDatePicker({ format : 'dddd DD/MM/YYYY',lang:'it',time: false, date:true });
							
							function modificaDataInizio_google(){
								$('#DataAppuntamento_google').hide();
								$('#data_google').show();
							}
							</script>
			</div>
			<div class="form-group" style="display:none">
				<label for="recipient-name" class="form-control-label">Ora inizio:</label>
				<input type="text" class="form-control" id="OraStartAppuntamento_google" onclick="modificaOraInizio_google()"></input>
				<input type="text" id="oraInizio_google" class="form-control" style="display:none" placeholder="Seleziona ora inizio">
							<script>
							$('#oraInizio_google').bootstrapMaterialDatePicker({ format : 'HH:mm',time: true, date:false });
							
							function modificaOraInizio_google(){
								$('#OraStartAppuntamento_google').hide();
								$('#oraInizio_google').show();
							}
							</script>
			</div>
			<div class="form-group" style="display:none">
				<label for="recipient-name" class="form-control-label">Ora fine:</label>
				<input type="text" class="form-control" id="OraEndAppuntamento_google" onclick="modificaOraFine_google()"></input>
				<input type="text" id="oraFine_google" class="form-control" style="display:none" placeholder="Seleziona ora fine">
				
							<script>
							$('#oraFine_google').bootstrapMaterialDatePicker({ format : 'HH:mm',time: true, date:false });
							
							function modificaOraFine_google(){
								$('#OraEndAppuntamento_google').hide();
								$('#oraFine_google').show();
							}
							</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Terapista:</label>
				<input type="text" class="form-control" id="TerapistaAppuntamento_google" disabled></input>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Stanza:</label>
				<input type="text" class="form-control" id="StanzaAppuntamento_mod2_google" onclick="modificaStanza_google()"></input>
				<select multiple class="form-control" id="StanzaAppuntamento_mod_google" style="display:none"></select>
				<script>
							function modificaStanza_google(){
								$('#StanzaAppuntamento_mod2_google').hide();
								$('#StanzaAppuntamento_mod_google').show();
							}
				</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Tariffa:</label>
				<input type="text" class="form-control" id="TariffaAppuntamento_mod2_google" onclick="modificaTariffa_google()"></input>
				<select multiple class="form-control" id="TariffaAppuntamento_mod_google" style="display:none"></select>
				<script>
							function modificaTariffa_google(){
								$('#TariffaAppuntamento_mod2_google').hide();
								$('#TariffaAppuntamento_mod_google').show();
							}
				</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Note:</label>
				<input type="text" class="form-control" id="NoteAppuntamento_google"></input>
			</div>
		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
        <button type="button" class="btn btn-primary" onclick="GOOGLESalvaEvento()">Salva</button>
		
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="InterrompiRicorrenza" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<h6 class="modal-title" id="exampleModalLabel">Elimina appuntamento</h6>-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Vuoi davvero interrompere la ricorrenza??</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-secondary" onclick="interrompiRicorrenza()"> Interrompi</button>
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modificaEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title" id="TitoloAppuntamento_mod"></h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Nome Paziente:</label>
				<input type="text" class="form-control" id="NomeAppuntamento_mod" disabled></input>
			</div>
			<div class="form-group" id="data_appuntamento">
				<label for="recipient-name" class="form-control-label">Data appuntamento:</label>
				<input type="text" class="form-control" id="DataAppuntamento_mod" onclick="modificaDataInizio()"></input>
				<input type="text" id="data_mod" class="form-control" style="display:none" placeholder="Seleziona data appuntamento">
							<script>
							$('#data_mod').bootstrapMaterialDatePicker({ format : 'dddd DD/MM/YYYY',lang:'it',time: false, date:true });
							
							function modificaDataInizio(){
								$('#DataAppuntamento_mod').hide();
								$('#data_mod').show();
							}
							</script>
			</div>
			<div class="form-group"  id="ora_inizio_appuntamento">
				<label for="recipient-name" class="form-control-label">Ora inizio:</label>
				<input type="text" class="form-control" id="OraStartAppuntamento_mod" onclick="modificaOraInizio()"></input>
				<input type="text" id="oraInizio_mod" class="form-control" style="display:none" placeholder="Seleziona ora inizio">
							<script>
							$('#oraInizio_mod').bootstrapMaterialDatePicker({ format : 'HH:mm',time: true, date:false });
							
							function modificaOraInizio(){
								$('#OraStartAppuntamento_mod').hide();
								$('#oraInizio_mod').show();
							}
							</script>
			</div>
			<div class="form-group" id="ora_fine_appuntamento">
				<label for="recipient-name" class="form-control-label">Ora fine:</label>
				<input type="text" class="form-control" id="OraEndAppuntamento_mod" onclick="modificaOraFine()"></input>
				<input type="text" id="oraFine_mod" class="form-control" style="display:none" placeholder="Seleziona ora fine">
				
							<script>
							$('#oraFine_mod').bootstrapMaterialDatePicker({ format : 'HH:mm',time: true, date:false });
							
							function modificaOraFine(){
								$('#OraEndAppuntamento_mod').hide();
								$('#oraFine_mod').show();
							}
							</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Terapista:</label>
				<input type="text" class="form-control" id="TerapistaAppuntamento_mod" onclick="modificaTerapista()" disabled></input>
				<select multiple class="form-control" id="TerapistaAppuntamento_mod2" style="display:none"></select>
				<script>
				function modificaTerapista(){
		  
    					if (username[0]=="1"){
    					$('#TerapistaAppuntamento_mod').hide();
    					$('#TerapistaAppuntamento_mod2').show();
		  
				}
				}
				
				</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Stanza:</label>
				<input type="text" class="form-control" id="StanzaAppuntamento_mod" onclick="modificaStanza()"></input>
				<select multiple class="form-control" id="StanzaAppuntamento_mod2" style="display:none"></select>
				<script>
				function modificaStanza(){
					$('#StanzaAppuntamento_mod').hide();
					$('#StanzaAppuntamento_mod2').show();
				}
				
				</script>
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Tariffa:</label>

				<div class="row">
								<div class="col-10">
									<input type="text" class="form-control" id="Tariffa_ric" disabled></input>
									<input type="number" class="form-control" id="Tariffa_ric_mod"
										style="display: none"></input> <span class="bmd-help">Inserire
										un importo valido (es. 10.00)</span>
								</div>
								<div class="col">
									<a href="#" onclick="inserisciTariffa_ric()" id="piu_ric"><i
										class="fa fa-plus-circle prefix"></i></a> <a href="#"
										onclick="nascondiTariffa_ric()" style="display: none" id="meno_ric"><i
										class="fa fa-minus-circle prefix"></i></a>
								</div>
								<script>
					function inserisciTariffa_ric(){
								$('#Tariffa_ric').hide();
								$('#piu_ric').hide();
								$('#Tariffa_ric_mod').show();
								$('#meno_ric').show();
							}
							
						function nascondiTariffa_ric(){
								$('#Tariffa_ric').show();
								$('#piu_ric').show();
								$('#Tariffa_ric_mod').hide();
								$('#meno_ric').hide();
							}
				
				</script>
				</div>

							<!-- <input type="text" class="form-control" id="TariffaAppuntamento_mod" onclick="modificaTariffa()"></input>
				<select multiple class="form-control" id="TariffaAppuntamento_mod2" style="display:none"></select>
				<script>
				function modificaTariffa(){
					$('#TariffaAppuntamento_mod').hide();
					$('#TariffaAppuntamento_mod2').show();
				}
				</script>
				 -->
			</div>
			<div class="form-group">
				<label for="recipient-name" class="form-control-label">Note:</label>
				<input type="text" class="form-control" id="NoteAppuntamento_mod"></input>
			</div>
							<!-- Div nascosto per modificare l'intera ricorrenza. Se il valore è 0 si modifica solo il singolo evento, se è 1 si modifica l'intera ricorrenza -->
			<div class="form-check form-check-inline" style="display:none">
			  <input type="text" class="form-control" id="modRicorrenza"></input>
			</div>																																			
		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cancellaModal('modificaEvento')">Chiudi</button>
              <button type="button" class="btn btn-primary" onclick="modificaEventoControlloTariffa()">Salva</button>
		
      </div>
	      		<script>
				
		function modificaEventoControlloTariffa(){
			
			if($('#Tariffa_ric_mod').val()!=""){
				
					var stato_tariffa = $("#meno_ric").is(":visible"); 
					if(stato_tariffa){
						inserisciQueryTariffa_ric($('#Tariffa_ric_mod').val());	
					} else {
						AGENDAupgradeEvento();
					}
				//AGENDAsalvaEvento($('#Tariffa_mod').val());
			} else {
				AGENDAupgradeEvento();
			}
		}
		</script>
    </div>
  </div>
</div>

<div class="modal fade" id="salvaEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crea nuovo appuntamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
      <div class="modal-body">
        <form>
		<div class="form-group">
            <label for="recipient-name" class="form-control-label">Titolo evento: <font color="red">(*) </font> </label>
            <input type="text" class="form-control" id="TitoloNuovoAppuntamento"></input>
		 </div>
		  <div class="form-group">
            <label for="recipient-name" class="form-control-label">Nome Paziente:</label>
            <input type="text" class="form-control" id="NomeNuovoAppuntamento" disabled></input>
          </div>
		  
		  <div class="form-group">
				<label class="form-control-label">Seleziona data appuntamento: </label>
				<div class="form-control-wrapper">
							<input type="text" id="date" class="form-control" disabled>
				</div>
		  </div>

		   <div class="form-group">
				<label class="form-control-label">Seleziona ora inizio appuntamento:<font color="red"> (*)</font></label>
				<div class="form-control-wrapper">
							<input type="text" id="hour" class="form-control">
							<script>
							$('#hour').bootstrapMaterialDatePicker({ format : 'HH:mm',time: true, date:false });
							</script>
				</div>
		  </div>
		  <div class="form-group">
            <label for="message-text" class="form-control-label">Durata:<font color="red"> (*)</font></label>
			<select class="form-control" id="ListaOre"></select>
          </div>
		  
		  
		 <div class="form-group">
			<label for="Ricorrenza">Ricorrenza</label>
			<select class="form-control" id="Ricorrenza">
			  <option>Nessuna</option>
			  <option>Giornaliera</option>
			  <option>Settimanale</option>
			  <option>Mensile</option>
			</select>
			<script> $("Ricorrenza").prop('required',true);</script>
		  </div>
		  
		  <div class="form-group" id="Fine_ricorrenza">
				<label class="form-control-label">Seleziona fine ricorrenza:<font color="red"> (*)</font></label>
				<div class="form-control-wrapper">
							<input type="text" id="date_ricorrenza" class="form-control">
							<script>
							
							$('#date').bootstrapMaterialDatePicker().on('change', function(e, date) {
							$('#date_ricorrenza').bootstrapMaterialDatePicker('setMinDate', date);
							
							
// 							var uno = $('#date').val();
// 							var due = $('#date_ricorrenza').val();
							
// 							if (due!="") {
							
// 									var uno1 = calcolaMs(uno,1);
// 									var due1 = calcolaMs(due,2);
							
// 									if(uno1>due1) {
// 									alert("ATTENZIONE! \nLa data di fine ricorrenza inserita non è valida!")
// 									}
// 							}
							});
							
							$('#date_ricorrenza').bootstrapMaterialDatePicker({ format : 'dddd DD/MM/YYYY',lang: 'it', weekStart : 1, cancelText : 'ANNULLA', time: false, disabledDays: [7]});
							
							
							$('#Ricorrenza').change(function(){
							switch($( "#Ricorrenza option:selected" ).text()) {
							
								case "Nessuna":
									$('#Fine_ricorrenza').hide();
									break;
								default:
									$('#Fine_ricorrenza').show();
							}
							});

							// controllo che la data di fine ricorrenza non sia antecedente al giorno dell'appuntamento

							$('#date_ricorrenza').bootstrapMaterialDatePicker().on('change', function(e, date) {
								
								var uno = $('#date').val();
								var due = $('#date_ricorrenza').val();
								
								if (due!="") {
								
										var uno1 = calcolaMs(uno,2);
										var due1 = calcolaMs(due,2);
								
										if(uno1>due1) {
										alert("ATTENZIONE! \nLa data di fine ricorrenza inserita non è valida!")
										}
								}
								});																																				   
						
							</script>
				</div>
		  </div>
	
		  <div class="form-group">
            <label for="message-text" class="form-control-label">Seleziona terapista:<font color="red"> (*)</font></label>
			<select class="form-control" id="ListaPersonale"></select>
			</div>
		  
		  <script>
							
							$('#ListaPersonale').change(function(){
							switch($( "#ListaPersonale option:selected" ).text()) {
								case " ":
									$('#div_stanza').hide();
									$('#div_tariffa').hide();
									
									break;
								default:
									TrovaStanzaTariffa($( "#ListaPersonale option:selected" ).text());
									$('#div_stanza').show();
									$('#div_tariffa').show();
							}
							});
												
							
		  </script>
		  
		 <div class="form-group" style="display:none" id="div_stanza">
            <label for="message-text" class="form-control-label">Stanza</label>
            <div class="row">
            	<div class="col-10">
            		<input type="text" class="form-control" id="ListaStanza" onclick="modificaStanza_salva()"></input>
            		<select multiple class="form-control" id="ListaStanza_mod" style="display:none"></select>
            	</div>
            </div>

				<script>

		function modificaStanza_salva(){
					$('#ListaStanza').hide();
					$('#ListaStanza_mod').show();
		}
				
				</script>

	</div>	
		  
		  <div class="form-group" style="display:none" id="div_tariffa">
            <label for="message-text" class="form-control-label">Tariffa</label>
			<div class="row">
				<div class="col-10">
				  <input type="text" class="form-control" id="Tariffa" disabled></input>
				  <input type="number" class="form-control" id="Tariffa_mod" style="display:none"></input>
				  <span class="bmd-help">Inserire un importo valido (es. 10.00)</span>
				</div>
				<div class="col">
				  <a href="#" onclick="inserisciTariffa()" id="piu"><i class="fa fa-plus-circle prefix"></i></a>
				  <a href="#" onclick="nascondiTariffa()" style="display:none" id="meno"><i class="fa fa-minus-circle prefix"></i></a>
				</div>
				<script>
					function inserisciTariffa(){
								$('#Tariffa').hide();
								$('#piu').hide();
								$('#Tariffa_mod').show();
								$('#meno').show();
							}
							
						function nascondiTariffa(){
								$('#Tariffa').show();
								$('#piu').show();
								$('#Tariffa_mod').hide();
								$('#meno').hide();
							}
				
				</script>
			</div>	
			
			
			
          </div>
		  
		   <div class="form-group">
				<label for="Note">Note</label>
				<textarea class="form-control" rows="2" id="NoteNuovoAppuntamento"></textarea>
			</div>
		<div class="form-group">
		<div class="form-check form-check-inline">
			  <input type="checkbox" id="mailPaziente" value="option1">
			  <label class="form-check-label" for="mailPaziente"> Invia email al paziente</label>
		</div>	  
		<div class="row">
				<div class="col-12">
				<font color="red" size="2"> (*) campi obbligatori </font>
				</div>
		</div>
		</div>
		
		
        </form>
      </div>
	  
	  
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="cancellaModal('salvaEvento')">Chiudi</button>
        <button type="button" class="btn btn-primary" onclick="salvaEventoControlloTariffa()">Crea</button>
		
		<script>
		function salvaEventoControlloTariffa(){
			
			if($('#Tariffa_mod').val()!=""){
				
					var stato_tariffa = $("#meno").is(":visible"); 
					if(stato_tariffa){
						inserisciQueryTariffa($('#Tariffa_mod').val());	
					} else {
					AGENDAsalvaEvento();
					}
				//AGENDAsalvaEvento($('#Tariffa_mod').val());
			} else {
				AGENDAsalvaEvento();
			}
		}
		</script>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="CercaPaziente">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TitoloCercaPaziente"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="larg2" class="modal-body">
         <table id="jqGrid"></table>
		 <div id="packagePager"></div>
		 <div id="packagePager1"></div>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" onclick="MostraModalNuovoAppuntamento()">Crea nuovo appuntamento</button>
		<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="inserisciPresenze">
  <div id="larg" class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="InserisciPresenzeModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <table id="gridPresenze"></table>
	  </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-outline-primary" data-dismiss="modal" onclick="SCUOLAcancellaPresenze()">Inserisci</button>
		<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="CercaPazienteGoogle">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TitoloCercaPazienteGoogle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="larg3" class="modal-body">
         <table id="jqGrid_google"></table>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-primary" onclick="inserisciPazienteGoogle()">Scegli paziente</button>
		
		<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>

<script> 
			function inserisciPazienteGoogle() {
				$('#CercaPazienteGoogle').modal('toggle');
				$('#NomeAppuntamento_google').val(nomeCognome_google);
			}
</script>
<div class="modal fade" id="EliminaEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<h6 class="modal-title" id="exampleModalLabel">Elimina appuntamento</h6>-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Vuoi davvero eliminare l'appuntamento?</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-secondary" onclick="EliminaEvento()"> Elimina</button>
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="GoogleSincronizzato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<h6 class="modal-title" id="exampleModalLabel">Elimina appuntamento</h6>-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Evento sincronizzato su Google!</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="EliminaRicorrenza" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<h6 class="modal-title" id="exampleModalLabel">Elimina appuntamento</h6>-->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Vuoi davvero eliminare la ricorrenza??</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-secondary" onclick="EliminaRicorrenza()"> Elimina</button>
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ConfermaEliminaEvento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Appuntamento eliminato correttamente</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="CompilaTuttiCampi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">ATTENZIONE, per poter creare l'evento è necessario compilare TUTTI i campi</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="RichiestaLoginGoogle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Questa operazione richiede l'accesso a Google</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
			   <button type="button" class="btn btn-primary"" data-dismiss="modal" onclick="handleClientLoad()">Login</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="contattaSupporto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Per utilizzare questa funzione contatta il supporto <br>via email:  <a href="mailto:andrea@moscatelliweb.it">andrea@moscatelliweb.it</a></p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
			</div>
        </div>
    </div>
</div>


<div class="modal fade" id="licenzaInScadenza" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">La tua licenza sta per scadere<br>Per rinnovare contatta il supporto <br>via email:  <a href="mailto:andrea@moscatelliweb.it">andrea@moscatelliweb.it</a></p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="noPazienti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Per creare un nuovo appuntamento devi prima inserire almeno un paziente</p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-primary"" data-dismiss="modal" onclick="MENUcallMenuItem1('anagrafica/anagrafica_cerca.html')">Inserisci</button>
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="presenzeInserite" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p align="center">Presenze aggiornate correttamente</a></p>
            </div>
            <div class="modal-footer">
			   <button type="button" class="btn btn-danger"" data-dismiss="modal">Chiudi</button>
			</div>
        </div>
    </div>
</div>


 


</body>

<script src='../jsNew/agenda_stanze.js'></script>

<!-- loading cdn -->
	<script src="../jsNew/loadingoverlay.min_1.6.0.js"></script>
	 
  <script src="../jsNew/loadingoverlay_progress.min_1.6.0.js"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign();
controllaStatus();
});
</script>



</html>