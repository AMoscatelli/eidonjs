<?php
error_reporting(E_ALL);

// decoding the json array
$post = json_decode(file_get_contents("php://input"), true);

// getting the information from the array
// in the android example I've defined only one KEY. You can add more KEYS to your app

//$my_value = "ciao";
$package = $post['package'];
$versione = $post['versione'];
$serial = $post['serial'];
$fw = $post['fw'];
// the "params1" is from the map.put("param1", "example"); in the android code
// if you make a "echo $my_value;" it will return a STRING value "example"

// Genera un boundary
$mail_boundary = "=_NextPart_" . md5(uniqid(time()));

$to = "andrea.moscatelli@guest.telecomitalia.it, alessio1.baroni@guest.telecomitalia.it, erica.vaccari@guest.telecomitalia.it, fabrizio.colombino@guest.telecomitalia.it, ruggiero.delcuratolo@guest.telecomitalia.it";
$subject = "Notifica di aggiornamento Applicazione TIMBOX S.N.: ".$serial;
$sender = "noreply@timbox.it";


$headers = "From: $sender\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-Type: multipart/alternative;\n\tboundary=\"$mail_boundary\"\n"; 
$headers .= "X-Mailer: PHP " . phpversion();

// Corpi del messaggio nei due formati testo e HTML
$text_msg = "messaggio in formato testo";
$html_msg = "prova mail";

// Costruisci il corpo del messaggio da inviare
$msg = "This is a multi-part message in MIME format.\n\n";
$msg .= "--$mail_boundary\n";
$msg .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
$msg .= "Content-Transfer-Encoding: 8bit\n\n";
$msg .= "Ciao\r\n";
$msg .= "\r\n";
$msg .= "Aggiornamento eseguito:\r\n";
$msg .= "\r\n";
$msg .= "<strong>INFO DEVICE:</strong>\r\n";
$msg .= "Firmware version: ".$fw."\r\n";
$msg .= "Package: ".$package."\r\n";
$msg .= "Nuova versione: ".$versione."\r\n";
$msg .= "\r\n";
$msg .= "<strong>Saluti da DM ALTRAN</strong>\r\n";// aggiungi il messaggio in formato text

$msg .= "\n--$mail_boundary\n";
$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
$msg .= "Content-Transfer-Encoding: 8bit\n\n";
$msg .= "Ciao\r\n";
$msg .= "\r\n";
$msg .= "Aggiornamento eseguito:\r\n";
$msg .= "\r\n";
$msg .= "<strong>INFO DEVICE:</strong>\r\n";
$msg .= "<strong>Firmware version:</strong> ".$fw."\r\n";
$msg .= "<strong>Package:</strong> ".$package."\r\n";
$msg .= "<strong>Nuova versione:</strong> ".$versione."\r\n";
$msg .= "\r\n";
$msg .= "<strong>Saluti da DM ALTRAN</strong>\r\n";// aggiungi il messaggio in formato text

// Boundary di terminazione multipart/alternative
$msg .= "\n--$mail_boundary--\n";

// Imposta il Return-Path (funziona solo su hosting Windows)
ini_set("sendmail_from", $sender);

// Invia il messaggio, il quinto parametro "-f$sender" imposta il Return-Path su hosting Linux
mail($to, $subject, $msg, $headers, "-f$sender");
echo '{"esito":"OK"}';

//phpinfo();
//echo $_SERVER['HTTP_X_FORWARDED_FOR'];


?>
