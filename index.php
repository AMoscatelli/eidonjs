<?php
require 'php/config.inc.php';
include 'php/category.php';


if(isset($_POST['accedi1'])) {
	
	setcookie('categorySelected', $cat1, time() + (86300), "/");
	setcookie('categoryUnselected', $cat, time() + (86300), "/");
	

	
	//header("Location: ../index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="cache-control" content="no-cache" />
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/igea.ico">
    <title>IGEA Ver:1.0</title>
    

	<script src='jsNew/jquery-2.2.4.min.js'></script>
	<script src='jsNew/tether.min.js'></script>

<!-- Inizializzo le notifiche -->
<link rel="manifest" href="notifications/manifest.json">

<script src="https://www.gstatic.com/firebasejs/5.5.3/firebase-app.js"></script>
<script
	src="https://www.gstatic.com/firebasejs/5.5.3/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.3/firebase.js"></script>

<script src='notifications/firebase.js'></script>
<!-- ::::::: -->

<link href="cssNew/bootstrap.min.css" rel="stylesheet">
	<link href="cssNew/custom_modal.css" rel="stylesheet"> 
	<link href="cssNew/dashboard.css" rel="stylesheet">
	<script src='jsNew/cookie.js'></script>
	<script src="jsNew/bootstrap.min_4.0.js"></script>
	<script async defer src="https://apis.google.com/js/api.js"></script>
	
	<script> 
	var googleAPI = "<?php echo $googleAPI; ?>"; 
	var client_id = "<?php echo $clientID;  ?>";
	</script>
	<script src='jsNew/google.js'></script>
	<script src="jsNew/menu.js"></script>
	<script language="javascript" type="text/javascript">
		window.onload = function(e){	
			checkCookie();
			
			
			var username = checkCookie();
			
			var cat1 = username[3];
			var cat2 = username[4];
			var catId = username[6].substring(0,username[6].indexOf("-"));
			var catName = username[6].substring(username[6].indexOf("-")+1,username[6].length);
			
			if(username[1]!=="1"){
				$('#infoBtn').hide();
			}
			
			if(catId!=""){
				$('#accedi').html("Accedi come "+catName.replace("+", " "));
				$('#accedi').show();
				$('#accedi').val(catId);
				
			}
			
			if($(location).attr('pathname').indexOf("index.php")>-1){
				console.log("ok");
			}else{
				window.open("index.php", "_self");
			}
		}

		</script>
    <script> window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    
	<style>
	nav#left-bar {
		display: none;
    }
	img#round {
		border-radius: 7px;
	}
	iframe#iframe {
		margin-top: 5px;
	}
	.scrollable-menu {
    height: auto;
    max-height: 650px;
    overflow-x: hidden;
    }
	</style>
  </head>

  <body>
    <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
      <button class="navbar-toggler navbar-toggler-right hidden-lg-up" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
	  <img class="hidden-lg-down" src="img/airri_small_50.png" alt="IGEA logo" height="42" width="42">
      <a class="navbar-brand" href="#">IGEA - Ver:1.0</a>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav">
		<!--<div class="btn-group">
		  <button type="button" class="btn btn-outline-success ml-2" onclick="MENUhomeMenu();">Home</button>
		</div>-->
 		<div class="btn-group">
		  <button id="calendario" type="button" class="btn btn-outline-success ml-2" onclick="(MENUcallMenuItem('agenda/calendar.php'))">Calendario</button>
		  <button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		  </button>

			<div class="dropdown-menu scrollable-menu" id="calendars">
			<button class="dropdown-item" value="99999" id="button_sync"><img src="agenda/images/calendar_icon.png" height="5%" width="5%">  Google Sync</button>
			<!-- introduco bottone per visualizzazione stanze -->
			<div class="dropdown-divider"></div>
			<button class="dropdown-item" value="9999" id="button_stanze"><img src="agenda/images/stanze.png"  height="5%" width="5%">  Mostra Stanze</button>
			<!-- fine bottone -->

			<div class="dropdown-divider"></div>
			</div>
		<div class="btn-group">
		  <button type="button" class="btn btn-outline-success ml-2" onclick="MENUshowMenu(anagrafica, 'anagrafica/anagrafica_cerca.html');">Anagrafica</button>
		</div>
		<div class="btn-group">
		  <button type="button" class="btn btn-outline-success ml-2" onclick="MENUshowMenu(terapisti,'terapisti/terapisti_show.html');">Terapisti</button>
		</div>
		<div class="btn-group">
		  <button id="utility" type="button" class="btn btn-outline-success ml-2">Utility</button>
		  <button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		  </button>
		  <div class="dropdown-menu" id="Stanze">
			<button class="dropdown-item" value="categoriaT" onclick="MENUshowMenu(anagrafica, 'utility/utility_categoriaT.html');">Categorie Terapisti</button>
			<div class="dropdown-divider"></div>
			<button class="dropdown-item" value="stanza" onclick="MENUshowMenu(anagrafica, 'utility/utility_stanza.html');" id="button_sync">Stanze</button>
			<div class="dropdown-divider"></div>
			<button class="dropdown-item" value="tariffa" onclick="MENUshowMenu(anagrafica, 'utility/utility_tariffa.html');">Tariffe</button>
			<div class="dropdown-divider"></div>
			<button class="dropdown-item" value="doposcuola" onclick="MENUshowMenu(anagrafica, 'doposcuola/doposcuola_add.html');">Dopo scuola</button>

		  </div>
		</div>
		<div class="btn-group">
		
		<button class="btn btn-outline-success ml-2 " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Report
		</button>
		<button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		  </button>
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
			<button class="dropdown-item" value="report_app" onclick="MENUshowMenu(null, 'stampa/stampa.html');">Appuntamenti</button>
			<div class="dropdown-divider"></div>
			<button class="dropdown-item" value="report_dop" onclick="MENUshowMenu(null, 'stampa/stampa_doposcuola.html');" id="button_sync">Doposcuola</button>
			
			</div>
		</div>
		
		<div class="btn-group">
		  <button type="button" id="infoBtn" class="btn btn-outline-success ml-2" onclick="MENUcallMenuItem('info/info.html');">Info</button>
		</div>
		<div class="btn-group">
		<button type="button" class="btn btn-outline-success ml-2" onclick="window.location='category.html'" name="accedi1" id="accedi"  style="display:none" >Accedi come </button>
		</div>
		<div class="btn-group">
		  <button id="logoutButton" type="button" class="btn btn-outline-danger ml-2" onclick="MENUchiamaModal()"></button>
			</div>
        </ul>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav id="left-bar" class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
		  <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="#" id="userName">Benvenuto: <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" onclick="MENU();">Esci</a>
            </li>
          </ul>
          <ul class="nav nav-pills flex-column"id="anagrafica">
            <li class="nav-item">
              <a class="nav-link active" href="#">Anagrafica<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('anagrafica/anagrafica_menu1.html')" href="#">Elenco</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('anagrafica/anagrafica_cerca.html')" href="#">Cerca</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('anagrafica/anagrafica_inserisci.html')" href="#">Inserisci</a>
            </li>
          </ul>
		  
		  <ul class="nav nav-pills flex-column"id="terapisti">
            <li class="nav-item">
              <a class="nav-link active" href="#">Terapisti<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('terapisti/terapisti_show.html')" href="#">Cerca</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('terapisti/terapisti_add.html')" href="#">Inserisci</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('terapisti/terapisti_varie.html')" href="#">Varie</a>
            </li>
          </ul>
		  <ul class="nav nav-pills flex-column"id="contabilita">
            <li class="nav-item">
              <a class="nav-link active" href="#">Contabilità<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('contabilita/contabilita_menu1.html');" href="#">Menu Contabilità 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('contabilita/contabilita_menu2.html');" href="#">Menu Contabilità 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="MENUcallMenuItem('contabilita/contabilita_menu3.html');" href="#">Menu Contabilità 3</a>
            </li>
          </ul>
		  <ul class="nav nav-pills flex-column"id="elaborazioni">
            <li class="nav-item">
              <a class="nav-link active" href="#">Elaborazioni<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Elaborazioni 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Elaborazioni 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Elaborazioni 3</a>
            </li>
          </ul>
		  <ul class="nav nav-pills flex-column"id="stampe">
            <li class="nav-item">
              <a class="nav-link active" href="#">Stampe<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Stampe 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Stampe 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Stampe 3</a>
            </li>
          </ul>
		  <ul class="nav nav-pills flex-column"id="analisi">
            <li class="nav-item">
              <a class="nav-link active" href="#">Analisi<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Analisi 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Analisi 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Menu Analisi 3</a>
            </li>
          </ul>
        </nav>
        <!--<main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">-->
          <!--<h1 id="Title">Dashboard</h1>-->
		  <div class="container-fluid" id="pippo"></div>
		  <div class="embed-responsive embed-responsive-16by9 mt-2">
			<iframe id="iframe" class="embed-responsive-item"  allowfullscreen></iframe>
		  </div>
		  <button onclick="topFunction()" id="myBtn" title="Go to top" style="display:none">Top</button>
        <!--</main>-->
      </div>
    </div>
	
<div class="modal fade bd-example-modal-sm" id="Logout" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog from-left" role="document">
	   <div class="modal-content">

            <div class="modal-body">
                <p align="center">Sicuro di voler uscire?</p>
			</div>
			<div class="modal-footer">
				<div class="col-sm-auto">
				   <button type="button" id="chiudi" class="btn btn-danger" data-dismiss="modal">Chiudi</button>
				   <button type="button" id="esci" class="btn btn-primary" data-dismiss="modal" onclick="MENUlogout()">Logout</button>
			   </div>
            </div>
            
        </div>
  </div>
</div>
	<script>
		MENUhomeMenu();
		MENUshowDopoScuola(checkCookie());
		$( document ).ready(function() {
			initFirebase("<?php echo $server_public_key; ?>","<?php echo $topic_name; ?>");
			createCSSSelector('#calendars', 'height: auto; max-height: 600px; overflow-x: hidden;');
		});
		
		function createCSSSelector (selector, style) {
			  if (!document.styleSheets) return;
			  if (document.getElementsByTagName('head').length == 0) return;

			  var styleSheet,mediaType;

			  if (document.styleSheets.length > 0) {
			    for (var i = 0, l = document.styleSheets.length; i < l; i++) {
			      if (document.styleSheets[i].disabled) 
			        continue;
			      var media = document.styleSheets[i].media;
			      mediaType = typeof media;

			      if (mediaType === 'string') {
			        if (media === '' || (media.indexOf('screen') !== -1)) {
			          styleSheet = document.styleSheets[i];
			        }
			      }
			      else if (mediaType=='object') {
			        if (media.mediaText === '' || (media.mediaText.indexOf('screen') !== -1)) {
			          styleSheet = document.styleSheets[i];
			        }
			      }

			      if (typeof styleSheet !== 'undefined') 
			        break;
			    }
			  }

			  if (typeof styleSheet === 'undefined') {
			    var styleSheetElement = document.createElement('style');
			    styleSheetElement.type = 'text/css';
			    document.getElementsByTagName('head')[0].appendChild(styleSheetElement);

			    for (i = 0; i < document.styleSheets.length; i++) {
			      if (document.styleSheets[i].disabled) {
			        continue;
			      }
			      styleSheet = document.styleSheets[i];
			    }

			    mediaType = typeof styleSheet.media;
			  }

			  if (mediaType === 'string') {
			    for (var i = 0, l = styleSheet.rules.length; i < l; i++) {
			      if(styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase()==selector.toLowerCase()) {
			        styleSheet.rules[i].style.cssText = style;
			        return;
			      }
			    }
			    styleSheet.addRule(selector,style);
			  }
			  else if (mediaType === 'object') {
			    var styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
			    for (var i = 0; i < styleSheetLength; i++) {
			      if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
			        styleSheet.cssRules[i].style.cssText = style;
			        return;
			      }
			    }
			    styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength);
			  }
			}
	</script>
	
  </body>
</html>
