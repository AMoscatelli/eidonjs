var rowData= [];
var rowData_t=[];
var idPaziente_cache;
var buttonAssocia;
var coockie=checkCookie(); 
var rowSelected = false;
var saveSelectedRows = [];

function initialMenu() {
	document.getElementById("aggiungi").style.display = "none";
	document.getElementById("delete").style.display = "none";
	document.getElementById("edit").style.display = "none";
	document.getElementById("update").style.display = "none";
	document.getElementById("editAssocia").style.display = "none";
	document.getElementById("viewAgenda").style.display = "none";
	document.getElementById("doposcuola").style.display = "none";
	document.getElementById("doposcuola_edit").style.display = "none";
}

function ANAGRAFICAinitGrid(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			showGrid(obj, GridName);
		  },
		  error: function () {
			alert("error");
		  }
	});
}


function showGrid(obj, GridName) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome", label: "Cognome Nome:"}
			
            
        ],
		autowidth: true,
        rowNum: 12,
		pager: "#packagePager",
        viewrecords: true,
        rownumbers: false,
		caption:"Cerca:",
        height: "auto",
        sortname: "nomeCognome",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid);
            rowSelected = true;
			idPaziente_cache=rowData.idPaziente;
			nome=rowData.nomeCognome;
			$("#nomeCognome").focus();
			$("#nomeCognome").val(rowData.nomeCognome);
			$("#eta").val(rowData.eta);
			$("#email").val(rowData.email);
			$("#sesso").val(rowData.sesso);
			$("#note").val(rowData.nota);
			$("#Patologia").val(rowData.Patologia);
			$("#start").val(rowData.start);
			$("#end").val(rowData.end);
			
			lockFields();
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("viewAgenda").style.display = "block"
			document.getElementById("delete").style.display = "block";
			document.getElementById("save").style.display = "none";
			document.getElementById("edit").style.display = "block";
			document.getElementById("update").style.display = "none";
		
			document.getElementById("editAssocia").style.display = "block";
		
			document.getElementById("assegna").style.display = "none";
			document.getElementById("doposcuola").style.display = "block";
			document.getElementById("doposcuola_edit").style.display = "none";
			document.getElementById("editAssocia").title = 'Terapisti Associati';
			
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

	
}

function lockFields(){
	document.getElementById("nomeCognome").readOnly = true;
	document.getElementById("eta").readOnly = true;
	document.getElementById("sesso").disabled=true; 
	document.getElementById("email").disabled = true;
	document.getElementById("note").readOnly = true;
	document.getElementById("Patologia").readOnly = true;
	document.getElementById("start").disabled = true;
	document.getElementById("end").disabled = true;

}	

function unlockFields(){
	document.getElementById("nomeCognome").readOnly = false;
	document.getElementById("eta").readOnly = false;
	document.getElementById("sesso").disabled=false; 
	document.getElementById("email").disabled = false;
	document.getElementById("note").readOnly = false;
	document.getElementById("Patologia").readOnly = false;
	document.getElementById("start").disabled =  false;
	document.getElementById("end").disabled =  false;

}	

function cancelFields() {
		document.getElementById("nomeCognome").value="";
		document.getElementById("eta").value="";
		document.getElementById("sesso").value="";
		document.getElementById("email").value="";
		document.getElementById("note").value="";
		document.getElementById("Patologia").value="";
		document.getElementById("start").value="";
		document.getElementById("end").value="";
		$("#nomeCognome").focus();

}



function ANAGRAFICAsalva(associa) {
	
	if (validate()) {
		var nomeCognome = $("#nomeCognome").val();
		var Query="INSERT INTO `paziente` (`nomeCognome`, `eta`, `email`, `nota`, `sesso`, `Patologia`, `start`, `end`) VALUES ('"+nomeCognome+"','"+$("#eta").val()+"','"+$("#email").val()+"','"+$("#note").val()+"','"+$("#sesso").val()+"','"+$("#Patologia").val()+"','"+$("#start").val()+"','"+$("#end").val()+"')";
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("edit").style.display = "block";
			document.getElementById("save").style.display = "none";
			document.getElementById("assegna").style.display = "none";
			document.getElementById("editAssocia").style.display = "block";
			document.getElementById("doposcuola").style.display = "block";
			lockFields();
			objID=JSON.parse(response);
			idPaziente_cache=objID[0].id;
			if(associa){
				ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
					showTerapistiModal();
			}else {
				window.parent.scrollTo(0, 0);
				$('#AssociazioneOK').modal();
			}
		  },
		  error: function () {
			alert("error");
		  }
		});	
	}else{	
	window.parent.scrollTo(0, 0);
		$('#validate').modal();
	}
}

function validate(){
	var nome_n=document.getElementById('nomeCognome').value;
	//var eta_n=document.getElementById('eta').value;
	var sesso_n=document.getElementById('sesso').value;
	//var mail_n=document.getElementById('email').value;
	//var nota_n=document.getElementById('note').value;
	//var patologia_n=document.getElementById('Patologia').value;
	//var start_n=document.getElementById('start').value;
	//var end_n=document.getElementById('end').value;
	if (nome_n=="" | sesso_n=="") {
		return false;
	}else{
		return true;
	}
}

function ANAGRAFICAmodalDelete() {
	window.parent.scrollTo(0, 0);
	$('#EliminaPaziente').modal();
}

function ANAGRAFICAdelete(){
	
	var Query_agenda= "SELECT t.nomeCognome_t , a.start FROM agenda AS a JOIN Terapisti AS t ON t.ID = a.idTerapista WHERE a.idPaziente = "+idPaziente_cache+ " AND a.stato != 3 AND a.eventoCancellato = 0";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query_agenda
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			if(obj.length!==0){
				//alert("Impossibile eliminare: il paziente ha appuntamenti pianificati in agenda")
				//$('#EliminaPaziente').modal('hide
				window.parent.scrollTo(0, 0);
				$('#ImpossibileEliminare').modal();
			}else{
				var Query= "DELETE FROM `paziente` WHERE `nomeCognome` = '"+$("#nomeCognome").val()+"' AND `eta` = '"+$("#eta").val()+"' AND `email` = '"+$("#email").val()+"' AND `sesso` = '"+$("#sesso").val()+"' AND`Patologia` = '"+$("#Patologia").val()+"' AND`start` = '"+$("#start").val()+"' AND `end` = '"+$("#end").val()+"'";
				$.ajax({
					  type: 'post',
					  url: '../php/mysql/Query_Insert_Delete.php',
					  data: { 
						'query': Query
					  },
					  success: function (response) {
						ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
						ANAGRAFICAadd();
						},
					  error: function () {
						alert("error");
					  }
					});
						}
						//showGrid(obj, GridName);
		  },
		  error: function () {
			//alert("error");
		  }
	});
	
}

function ANAGRAFICAadd(){
	
	unlockFields();
	cancelFields();
	initialMenu();
	document.getElementById("save").style.display = "block";
	document.getElementById("assegna").style.display = "block";
}

function ANAGRAFICAedit() {
	initialMenu();
	document.getElementById("update").style.display = "block";
	document.getElementById("editAssocia").style.display = "block";
	document.getElementById("save").style.display = "none";
	document.getElementById("assegna").style.display = "none";
	document.getElementById("editAssocia").title = 'Associa Terapista';
	document.getElementById("doposcuola_edit").style.display = "block";
	unlockFields();
}

function ANAGRAFICAmodifica() {
		
	if (validate) {
		
		var Query= "UPDATE `paziente` SET `nomeCognome` = '"+$("#nomeCognome").val()+"' , `eta` = '"+$("#eta").val()+"' , `email` = '"+$("#email").val()+"' , `sesso` = '"+$("#sesso").val()+"' ,`Patologia` = '"+$("#Patologia").val()+"',`start` = '"+$("#start").val()+"', `end` = '"+$("#end").val()+"' WHERE `paziente`.`idPaziente` = "+rowData.idPaziente;
		
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  
			ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
			
		  },
		  error: function () {
			alert("error");
		  }
	});
	}else{
		window.parent.scrollTo(0, 0);
		$('#validate').modal();
	}
	document.getElementById("aggiungi").style.display = "block";
	document.getElementById("delete").style.display = "block";
	document.getElementById("edit").style.display = "block";
	document.getElementById("update").style.display = "none";
	document.getElementById("doposcuola_edit").style.display = "none";
	document.getElementById("doposcuola").style.display = "block";
	document.getElementById("editAssocia").style.display = "block";
	document.getElementById("viewAgenda").style.display = "block";
	document.getElementById("editAssocia").title = 'Terapista Associato';
	lockFields();
	
}

function ANAGRAFICAassocia() {
		if(validate()){
			ANAGRAFICAsalva(true);
		}else{
			window.parent.scrollTo(0, 0);
			$('#validate').modal();
		}
		
}
//Lista MODAL

function showTerapistiModal(deleteT) {
	window.parent.scrollTo(0, 0);
	$('#associaTerapista').modal();
	$('#associaTerapista').on('shown.bs.modal', function  (e) { 
         //var w=$("#larg").innerWidth();
		 showTerapistiModal1(deleteT,$("#larg").innerWidth()-40); 

    });
	
}

function showTerapistiModal1(deleteT,w){
			var nome=document.getElementById("nomeCognome").value
			$('#nomePaziente').val(nome);
			if (deleteT==true){
				query="SELECT t.nomeCognome_t, t.ID FROM joinPazienteTariffaTerapista AS j JOIN Terapisti AS t ON t.ID = j.idTerapista WHERE j.IdPaziente = "+idPaziente_cache;
				TERAPISTIshowInitGrid(query, 'jqGridTerapist',w,undefined)
				deleteT=false;
			}else{
				
				if (coockie[0]==1){
					TERAPISTIshowInitGrid('Select * from Terapisti WHERE ID!=1', 'jqGridTerapist',w);
				}else{
					TERAPISTIshowInitGrid('Select * from Terapisti WHERE ID='+coockie[1], 'jqGridTerapist',w);
				}
			}
				
				
			//query="Select idTerapista from `joinPazienteTariffaTerapista` WHERE `idPaziente`="+rowData.idPaziente;
			
}

function TERAPISTIshowInitGrid(Query, GridName, w, obj_new){
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			buttonAssocia=undefined;
			if(obj_new!==undefined){
				//modalità visualizzazione
				if (document.getElementById("edit").style.display == "block"){
					for(var i = obj.length; i>0; i--){
						var a=0;
						for(var k =0; k<obj_new.length;k++){
							if(obj[i-1].ID==obj_new[k].idTerapista){
								a=1;
							}
						}
						if (a!==1){
							obj.splice([i-1],1);
							
						}
					}
				buttonAssocia=true;
				}else{
					//modalità edit
						for(var i = obj.length; i>0; i--){
							for(var k =0; k<obj_new.length;k++){
								if(obj[i-1].ID===obj_new[k].idTerapista){
									obj.splice([i-1],1);
								break;
								}
							}
						}buttonAssocia=undefined;
					}
				
				
			}
			if (obj.length!==0){
				TERAPISTIshowGrid(obj, GridName,w);
			}else {
				$('#jqGridTerapist').jqGrid('GridUnload');
			}
			
			
			if(buttonAssocia===undefined){
				if(obj.length!==0){
						document.getElementById("AssociaButton").style.display="block";
				}else{
						document.getElementById("AssociaButton").style.display="none";
				}
				
				document.getElementById("rimuoviT").style.display="none";	
			}else if(buttonAssocia){
				if(obj.length!==0){
						document.getElementById("rimuoviT").style.display="block";
				}else{
						document.getElementById("rimuoviT").style.display="none";
				}
				document.getElementById("AssociaButton").style.display="none";
				
			} 
			
		  },
		  error: function () {
			alert("error");
		  }
	});
}	



function TERAPISTIshowGrid(obj, GridName,w) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome_t", label: "Elenco Terapisti"}],
		width: w,
		caption:"Cerca:",
		autowidth: false,
		shrinkToFit: true,
		rowNum: 5,
		pager: "#packagePager1",
        viewrecords: true,
        rownumbers: false,
		//height: "auto",
        autoencode: true,
        ignoreCase: true,
        multiselect: true,
        onSelectRow: function (rowid) {
            rowData_t.push($(this).jqGrid("getLocalRow", rowid));
			saveSelectedRows = $(this).getGridParam('selarrrow');
        },
		
		gridComplete: function () {

            var currentPage = $(this).getGridParam('page').toString();
			console.log(saveSelectedRows);
			console.log(rowData_t);
            //retrieve any previously stored rows for this page and re-select them
            //var retrieveSelectedRows = $(this).data(currentPage);
            if (saveSelectedRows) {
                $.each(saveSelectedRows, function (index, value) {
                    $('#gridTerapisti').setSelection(value, false);
                });
            }
        }
    });
    $grid.jqGrid("navGrid", "#packagePager1",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
	
	
}

function ANAGRAFICAjoin(){
	selRowId_t_selected = rowData_t;//$('#jqGridTerapist').jqGrid ('getGridParam', 'selarrrow');
	if (selRowId_t_selected.length!==undefined){
		start="INSERT INTO `joinPazienteTariffaTerapista` (`idPaziente`, `idTariffa`, `idTerapista`, `idStanza`) VALUES"
		for (var i=0; i<selRowId_t_selected.length; i++){
			rowData_t=selRowId_t_selected[i];
			if (i===0){
				Query=start+"('"+idPaziente_cache+"','"+rowData_t.idTariffa+"','"+rowData_t.ID+"','"+rowData_t.idStanza+"')";
			}else{
				Query=Query+",('"+idPaziente_cache+"','"+rowData_t.idTariffa+"','"+rowData_t.ID+"','"+rowData_t.idStanza+"')";
			}
		}	
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				var x = document.getElementById("dw-s2").getAttribute("aria-expanded"); 
				if (x == "false") {
					window.parent.document.getElementById('clickDrower').click();  
				}
				window.parent.scrollTo(0, 0);
				$('#AssociazioneOK').modal();
				//ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
				//alert(response);
				
			  },
			  error: function () {
				alert("error");
			  }
		});
	}
	document.getElementById("editAssocia").title = 'Terapisti Associati';	
	$('#associaTerapista').modal('hide');
}
function ANAGRAFICAmodalEdit() {
	window.parent.scrollTo(0, 0);
	$('#associaTerapista').modal();
	$('#associaTerapista').on('shown.bs.modal', function  (e) { 
         //var w=$("#larg").innerWidth();
		 ANAGRAFICAmodalEdit1($("#larg").innerWidth()-40); 

    });
	
}
function ANAGRAFICAmodalEdit1(w) {
	
	var nome=document.getElementById("nomeCognome").value
	$('#nomePaziente').val(nome);
	if (coockie[0]==1){
		Query="Select idTerapista from `joinPazienteTariffaTerapista` WHERE `idPaziente`="+idPaziente_cache;
	}else{
		Query="Select idTerapista from `joinPazienteTariffaTerapista` WHERE `idPaziente`="+idPaziente_cache+" AND `idTerapista`="+coockie[1];
	}
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			//var idTerapisti=[];
			if (coockie[0]==1){
				TERAPISTIshowInitGrid('Select * from Terapisti WHERE ID!=1', 'jqGridTerapist',w,obj);
			}else{
				TERAPISTIshowInitGrid('Select * from Terapisti WHERE ID='+coockie[1], 'jqGridTerapist',w,obj);
			}
			},
		  error: function () {
			alert("error");
		  }
	});
	
	
}

function ANAGRAFICAremoveT(){
	selRowId_t_selected = $('#jqGridTerapist').jqGrid ('getGridParam', 'selarrrow');
	if (selRowId_t_selected.length!==undefined){		
		for (var i=0; i<selRowId_t_selected.length; i++){
			rowData_t=$('#jqGridTerapist').jqGrid("getLocalRow", selRowId_t_selected[i])
			
				Query="DELETE FROM `joinPazienteTariffaTerapista` WHERE `joinPazienteTariffaTerapista`.`idPaziente` ="+idPaziente_cache+" AND `joinPazienteTariffaTerapista`.`idTerapista` ="+rowData_t.ID+";";
				$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				  if(i==selRowId_t_selected.length){
					  window.parent.scrollTo(0, 0);
						$('#associaTerapista').modal('hide');
						if (document.getElementById("dw-s2").getAttribute("aria-expanded") == "false") {
							window.parent.document.getElementById('clickDrower').click();  
						}  
						window.parent.scrollTo(0, 0);
						$('#AssociazioneOK').modal();
						document.getElementById("editAssocia").title = 'Terapisti Associati';
				  }				
			  },
			  error: function () {
				alert("error");
			  }
		});
		}	
		
	}
		
	
}

function viewAGENDA(){
	
	var nome=document.getElementById("nomeCognome").value
	$('#nomePazienteAGENDA').val(nome);
	Query="SELECT start, title, nomeCognome_t as nome FROM agenda as a JOIN Terapisti as t ON t.ID = a.idTerapista WHERE idPaziente = "+idPaziente_cache+" and eventoCancellato = 0 and stato != 3";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);	
			AGENDAshowInitGrid(obj,'agenda');
			//AGENDAcalendar(obj);
			window.parent.scrollTo(0, 0);
			$('#agendaModal').modal();
			},
		  error: function () {
			alert("error");
		  }
	});
}


function AGENDAshowInitGrid(obj, GridName) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "start", label: "Data", formatter : 'date', formatoptions : {newformat : 'd-m-Y'}, height: 20},
			{ name: "start", label: "Ora", formatter : 'date', formatoptions : {srcformat : 'Y-m-d H:i:s', newformat : 'H:i:s'} , height: 20},
			{ name: "nome", label: "Terapista", height: 20},
            { name: "title", label: "Descrizione", height: 20},
			 { name: '', index: '',label: "Visualizza", sortable: false, formatter: Visualizza, align: "center" , height: 20},
        ],
		autowidth: true,
        caption:"Cerca:",
		rowNum: 6,
		pager: "#packagePager_agenda",
        viewrecords: true,
        rownumbers: false,
		//caption:"Cerca:",
        
        sortname: "nomeCognome",
        autoencode: true,
        //gridview: true,
        ignoreCase: true,
		
        
    });
	
	 
	
    $grid.jqGrid("navGrid", "#packagePager_agenda",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

	
}
function Visualizza(cellvalue, options, rowObject) {

        Data=convertiData(rowObject.start,"anno2");
		Ora=convertiData(rowObject.start,"ora");
		format='"'+Data+'","'+Ora+'"';
				//format='"'+rowObject.start+'"';
		html= "<button id='aggiungi' type='button' class='pull-center btn btn-danger bmd-btn-fab-sm'data-placement='top' data-toggle='tooltip' title='Visualizza in Agenda' onclick='pippo("+format+")'><i class='material-icons'>perm_contact_calendar</i></button>";
		//html= "<i class='material-icons'>perm_contact_calendar</i>";
		return html;
         
    }

function pippo(a,b){
	c='agenda/calendar.php?DATE='+a+'&Ora='+b;
	MENUcallMenuItem1(c);
}

function ANAGRAFICAmodalEditDS(deleteT){
	window.parent.scrollTo(0, 0);
	$('#doposcuolaModal').modal();
	$('#doposcuolaModal').on('shown.bs.modal', function (e) { 
				w = $("#larg1").innerWidth()-40;
				ANAGRAFICAmodalEditDS1(deleteT,w);

});
}
function ANAGRAFICAmodalEditDS1(deleteT,w){
			buttonAssocia=undefined;
			
			if (deleteT==true){
				//RIMUOVI DOPOSCUOLA 
				query="SELECT idDopoScuola,d.nomeDopoScuola FROM joinPazienteDopoScuola as j JOIN dopoScuola as d ON d.ID = j.idDopoScuola WHERE IdPaziente = "+idPaziente_cache;
				buttonAssocia=true;
				
					ANAGRAFICAds(query,buttonAssocia,w);
				
				deleteT=false;
			}else{
				//ASSOCIA DOPOSCUOLA IN EDIT
				buttonAssocia=undefined;
				ANAGRAFICAds("SELECT ID,nomeDopoScuola FROM `dopoScuola` WHERE ID NOT IN (SELECT idDopoScuola FROM joinPazienteDopoScuola as j JOIN dopoScuola as d ON d.ID = j.idDopoScuola WHERE IdPaziente ="+idPaziente_cache+")",buttonAssocia,w);
				
			}
			
}

function ANAGRAFICAds(Query, buttonAssocia,w){
	
	var nome=document.getElementById("nomeCognome").value
	$('#nomePazienteDOPOSCUOLA').val(nome);
	//Query="SELECT d.nomeDopoScuola FROM joinPazienteDopoScuola as j JOIN dopoScuola as d ON d.ID = j.idDopoScuola WHERE IdPaziente = "+idPaziente_cache;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);	
			
			//AGENDAcalendar(obj);
//			$('#doposcuolaModal').modal();
			DOPOSCUOLAshowGrid(obj,'DopoScuolaTable',w);
			if(buttonAssocia===undefined){
				if(obj.length!==0){
						document.getElementById("AssociaButtonDS").style.display="block";
				}else{
						document.getElementById("AssociaButtonDS").style.display="none";
				}
				
				document.getElementById("rimuoviDS").style.display="none";	
			}else if(buttonAssocia){
				if(obj.length!==0){
						document.getElementById("rimuoviDS").style.display="block";
				}else{
						document.getElementById("rimuoviDS").style.display="none";
				}
				document.getElementById("AssociaButtonDS").style.display="none";
				
			}
			
			},
		  error: function () {
			alert("error");
		  }
	});
}

function DOPOSCUOLAshowGrid(obj, GridName,w ) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
			{ name: "nomeDopoScuola", label: "Nome Doposcuola"}
        ],
		height:'auto',
		caption:"Cerca:",
		width: w,
		autowidth: false,
		rowNum: 12,
		pager: "#packagePager_ds",
        viewrecords: true,
        rownumbers: false,
        shrinkToFit: true,
        sortname: "nomeCognome",
        autoencode: true,
        ignoreCase: true,
        multiselect: true,
        onSelectRow: function (rowid) {
            rowData_t = $(this).jqGrid("getLocalRow", rowid);
        }
		
        
    });
	
	
	
    $grid.jqGrid("navGrid", "#packagePager_ds",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

}

function ANAGRAFICAjoinDS(){
	//EDIT
	selRowId_t_selected = $('#DopoScuolaTable').jqGrid ('getGridParam', 'selarrrow');
	if (selRowId_t_selected.length!==undefined){
		start="INSERT INTO `joinPazienteDopoScuola` (`IdPaziente`, `idDopoScuola`, `presenza`) VALUES"
		for (var i=0; i<selRowId_t_selected.length; i++){
			rowData_t=$('#DopoScuolaTable').jqGrid("getLocalRow", selRowId_t_selected[i])
			if (i===0){
				Query=start+"('"+idPaziente_cache+"','"+rowData_t.ID+"',0)";
			}else{
				Query=Query+",('"+idPaziente_cache+"','"+rowData_t.ID+"',0)";
			}
		}	
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				var x = document.getElementById("dw-s2").getAttribute("aria-expanded"); 
				if (x == "false") {
					window.parent.document.getElementById('clickDrower').click();  
				}
				window.parent.scrollTo(0, 0);
				$('#AssociazioneOK').modal();
				//ANAGRAFICAinitGrid('Select * from paziente', 'jqGrid');
				//alert(response);
				
			  },
			  error: function () {
				alert("error");
			  }
		});
	}
	//document.getElementById("editAssocia").title = 'Terapisti Associati';	
	$('#doposcuolaModal').modal('hide');
}


function ANAGRAFICAremoveDS(){
	selRowId_t_selected = $('#DopoScuolaTable').jqGrid ('getGridParam', 'selarrrow');
	if (selRowId_t_selected.length!==undefined){		
		for (var i=0; i<selRowId_t_selected.length; i++){
			rowData_t=$('#DopoScuolaTable').jqGrid("getLocalRow", selRowId_t_selected[i])
			
				Query="DELETE FROM `joinPazienteDopoScuola` WHERE `IdPaziente` ="+idPaziente_cache+" AND `idDopoScuola` ="+rowData_t.idDopoScuola+";";
				$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				  if(i==selRowId_t_selected.length){
						$('#doposcuolaModal').modal('hide');
						if (document.getElementById("dw-s2").getAttribute("aria-expanded") == "false") {
							window.parent.document.getElementById('clickDrower').click();  
						}  
						window.parent.scrollTo(0, 0);
						$('#AssociazioneOK').modal();
						//document.getElementById("editAssocia").title = 'Terapisti Associati';
				  }				
			  },
			  error: function () {
				alert("error");
			  }
		});
		}	
		
	}
		
	
}

