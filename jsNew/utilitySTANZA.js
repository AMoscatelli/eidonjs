var rowData= [];
var idStanza_cache;
function initialMenu() {
	document.getElementById("aggiungi").style.display = "none";
	document.getElementById("delete").style.display = "none";
	document.getElementById("edit").style.display = "none";
	document.getElementById("update").style.display = "none";
	
}

function UTILITYSTANZAinitGrid(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			showGrid(obj, GridName);
		  },
		  error: function () {
			alert("error");
		  }
	});
}


function showGrid(obj, GridName) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "stanza", label: "Nome Stanza"}
			
            
        ],
		autowidth: true,
        rowNum: 12,
		pager: "#packagePager",
        viewrecords: true,
        rownumbers: false,
		caption:"Cerca:",
        height: "auto",
        sortname: "nomeCognome",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid);
            
			idStanza_cache=rowData.ID;
			nome=rowData.stanza;
			$("#nomeStanza").focus();
			$("#nomeStanza").val(rowData.stanza);
			$("#coloreSelect").val(rowData.color);
						
			lockFields();
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("save").style.display = "none";
			document.getElementById("edit").style.display = "block";
			document.getElementById("update").style.display = "none";
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

	
}
function lockFields(){
	document.getElementById("nomeStanza").readOnly = true;
	document.getElementById("coloreSelect").readOnly = true;
	

}	

function UtilitySTANZAedit(){
	initialMenu();
	document.getElementById("update").style.display = "block";
	document.getElementById("save").style.display = "none";
	unlockFields();
}

function unlockFields(){
	document.getElementById("nomeStanza").readOnly = false;
	document.getElementById("coloreSelect").readOnly = false;
}	

function UtilitySTANZAadd(){
	
	unlockFields();
	cancelFields();
	initialMenu();
	document.getElementById("save").style.display = "block";
}

function cancelFields() {
		document.getElementById("nomeStanza").value="";
		$("#nomeStanza").focus();
}

function UtilitySTANZAsalva() {
	
	if (validate()) {
		var Query="INSERT INTO `Stanza` (`ID`, `stanza`, `color`) VALUES (NULL, '"+$("#nomeStanza").val()+"', '"+$("#coloreSelect").val()+"')";
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			UTILITYSTANZAinitGrid('Select * from Stanza', 'jqGrid');
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("edit").style.display = "block";
			document.getElementById("save").style.display = "none";
			lockFields();
			objID=JSON.parse(response);
			idStanza_cache=objID[0].ID;
			$('#AssociazioneOK').modal();
		  },
		  error: function () {
			alert("error");
		  }
		});	
	}else{	
		$('#validate').modal();
	}
}

function validate(){
	var nomeS_n=document.getElementById('nomeStanza').value;
	if (nomeS_n=="") {
		return false;
	}else{
		return true;
	}
}

function UtilitySTANZAmodalDelete() {
	$('#EliminaStanza').modal();
}


function UtilitySTANZAdelete(){
	
	var Query_agenda= "SELECT t.nomeCognome_t FROM Terapisti AS t JOIN Stanza AS s ON t.idStanza=s.ID WHERE s.ID = "+idStanza_cache;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query_agenda
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			if(obj.length!==0){
				//alert("Impossibile eliminare: il paziente ha appuntamenti pianificati in agenda")
				//$('#EliminaPaziente').modal('hide');
				$('#ImpossibileEliminare').modal();
			}else{
				var Query= "DELETE FROM `Stanza` WHERE `stanza` = '"+$("#nomeStanza").val()+"' AND `ID` = '"+idStanza_cache+"'";
				$.ajax({
					  type: 'post',
					  url: '../php/mysql/Query_Insert_Delete.php',
					  data: { 
						'query': Query
					  },
					  success: function (response) {
						UTILITYSTANZAinitGrid('Select * from Stanza', 'jqGrid');
						UTILITYdeleted();
						},
					  error: function () {
						alert("error");
					  }
					});
						}
						//showGrid(obj, GridName);
		  },
		  error: function () {
			//alert("error");
		  }
	});
	
}
function UTILITYdeleted(){
	
	unlockFields();
	cancelFields();
	initialMenu();
	document.getElementById("save").style.display = "block";
}

function UtilitySTANZAmodifica() {
		
	if (validate) {
		
		var Query= "UPDATE `Stanza` SET `stanza` = '"+$("#nomeStanza").val()+"' ,  `color` = '"+$("#coloreSelect").val()+"' WHERE `ID` = "+rowData.ID;
		
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  
			UTILITYSTANZAinitGrid('Select * from Stanza', 'jqGrid');
			
		  },
		  error: function () {
			alert("error");
		  }
	});
	}else{
	
		$('#validate').modal();
	}
	document.getElementById("aggiungi").style.display = "block";
	document.getElementById("delete").style.display = "block";
	document.getElementById("edit").style.display = "block";
	document.getElementById("update").style.display = "none";
	lockFields();
	
}
