var role;

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for(var i = 0; i <ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie() {
	var username = getCookie("eidonUserLogged");
	var cat1 = getCookie("categoryUser1");
	var cat2 = getCookie("categoryUser2");
	var catSelected = getCookie("categorySelected");
	var catUnselected = getCookie("categoryUnselected");
	
	if (username != "") {
		var email = username.substring(username.indexOf("|")+1, username.lastIndexOf("|"));
		var role = username.substring(username.length-1, username.length);
		var user = username.substring(0, username.indexOf("|"));
		
		var variabili = [role,user,email,cat1,cat2,catSelected,catUnselected];
		return variabili;
	} else {
		window.open('login.html', '_self', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
	}
}

window.onload = function(e){ 
	checkCookie(); 
}	

		
	