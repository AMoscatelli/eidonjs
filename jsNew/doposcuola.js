var obj = [];
var rowData;
var p;
var idDopoScuolaSelected;
var idTerapistaSelected;
var allEmpty = true;
var lun;
var mar;
var mer;
var gio;
var ven;
var sab;
var dom;
var value = "";
var daDisassociare = new Map();


function DOPOSCUOLAinitGrid(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			console.log(Object.keys(obj).length);
			if(Object.keys(obj).length !== 0){
				lockFields();
				showGridDS(obj, GridName);
				lockFields();
			}else{
				DOPOSCUOLAinitGrid_te('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid');
			}
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function DOPOSCUOLAinitGrid_te(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			showGrid(obj, GridName);
			unLockFields();
			document.getElementById("floatButtonEdit").style.display = "none";
			document.getElementById("floatButtonAdd").style.display = "none";
			document.getElementById("floatButtonDelete").style.display = "none";
			document.getElementById("floatButtonSave").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";
			},
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function DOPOSCUOLAinitGrid1(Query, GridName, add){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {

			obj = JSON.parse(response);
			showGrid1(obj, GridName);
			unLockFields();
			if(add){
				$("#nome").focus();
				$("#nome").val("");
				$("#terapista").val("");
				$("#start").val("");
				$("#end").val("");
				$("#lun_start").val("");
				$("#lun_stop").val("");
				$("#mar_start").val("");
				$("#mar_stop").val("");
				$("#mer_start").val("");
				$("#mer_stop").val("");
				$("#gio_start").val("");
				$("#gio_stop").val("");
				$("#ven_start").val("");
				$("#ven_stop").val("");
				$("#sab_start").val("");
				$("#sab_stop").val("");
				$("#dom_start").val("");
				$("#dom_stop").val("");
				document.getElementById("floatButtonEdit").style.display = "none";
				document.getElementById("floatButtonAdd").style.display = "none";
				document.getElementById("floatButtonDelete").style.display = "none";
				document.getElementById("floatButtonSave").style.display = "block";
				document.getElementById("floatButtonPazNonAss").style.display = "none";
				document.getElementById("floatButtonPazAss").style.display = "none";
				document.getElementById("associaButton").style.display = "none";
				document.getElementById("disassociaButton").style.display = "block";
				document.getElementById("floatButtonUpdate").style.display = "none";
			}else{
				unLockFields();
				$("#nome").focus();
				document.getElementById("floatButtonEdit").style.display = "none";
				document.getElementById("floatButtonAdd").style.display = "none";
				document.getElementById("floatButtonDelete").style.display = "none";
				document.getElementById("floatButtonSave").style.display = "none";
				document.getElementById("floatButtonPazNonAss").style.display = "block";
				document.getElementById("floatButtonPazAss").style.display = "none";
				document.getElementById("associaButton").style.display = "block";
				document.getElementById("disassociaButton").style.display = "none";
				document.getElementById("floatButtonUpdate").style.display = "block";
			}
			
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function showGrid1(obj, GridName) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome_t", label: "Cognome Nome:", width: 110 }
            
        ],
		autowidth: true,
		rowNum: 12,
        pager: "#jqGridPager",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeCognome_t",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
            for (p in rowData) {
                if (rowData.hasOwnProperty(p)) {
                    str += "propery \"" + p + "\" + have the value \"" + rowData[p] + "\n";
                }
            }
			$("#nome").focus();
			$("#terapista").val(rowData.nomeCognome_t);
			idTerapistaSelected = rowData.ID;
			//lockFields();
			/*document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";*/
 
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

function showGrid(obj, GridName) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome_t", label: "Cognome Nome:", width: 110 }
            
        ],
		autowidth: true,
		rowNum: 12,
        pager: "#jqGridPager",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeCognome_t",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
            for (p in rowData) {
                if (rowData.hasOwnProperty(p)) {
                    str += "propery \"" + p + "\" + have the value \"" + rowData[p] + "\n";
                }
            }
			$("#nome").focus();
			$("#terapista").val(rowData.nomeCognome_t);
			//lockFields();
			/*document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";*/
 
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

function showGridDS(obj, GridName) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeDopoScuola", label: "Dopo scuola:", width: 110 }
            
        ],
		autowidth: true,
		rowNum: 12,
        pager: "#jqGridPager",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeDopoScuola",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
            for (p in rowData) {
                if (rowData.hasOwnProperty(p)) {
                    str += "propery \"" + p + "\" + have the value \"" + rowData[p] + "\n";
                }
            }
			$("#nome").focus();
			$("#nome").val(rowData.nomeDopoScuola);
			$("#terapista").val(rowData.nomeCognome_t);//rowData.idTerapista);
			$("#start").val(rowData.start);
			$("#end").val(rowData.end);
			$("#stanzaSelect").val(rowData.idStanza);
			$("#tariffaSelect").val(rowData.idTariffa);
			$("#lun_start").val(splitStart(rowData.lun));
			$("#lun_stop").val(splitEnd(rowData.lun));
			$("#mar_start").val(splitStart(rowData.mar));
			$("#mar_stop").val(splitEnd(rowData.mar));
			$("#mer_start").val(splitStart(rowData.mer));
			$("#mer_stop").val(splitEnd(rowData.mer));
			$("#gio_start").val(splitStart(rowData.gio));
			$("#gio_stop").val(splitEnd(rowData.gio));
			$("#ven_start").val(splitStart(rowData.ven));
			$("#ven_stop").val(splitEnd(rowData.ven));
			$("#sab_start").val(splitStart(rowData.sab));
			$("#sab_stop").val(splitEnd(rowData.sab));
			$("#dom_start").val(splitStart(rowData.dom));
			$("#dom_stop").val(splitEnd(rowData.dom));
			idDopoScuolaSelected = rowData.ID;
			//lockFields();
			document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";
			document.getElementById("floatButtonPazAss").style.display = "block";
			document.getElementById("floatButtonPazNonAss").style.display = "none";
			document.getElementById("associaButton").style.display = "block";
			document.getElementById("disassociaButton").style.display = "none";
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

function splitStart(weekDay){
	return weekDay.substring(0, weekDay.indexOf("|"));
}
function splitEnd(weekDay){
	return weekDay.substring(weekDay.indexOf("|")+1, weekDay.length);
}
	
function lockFields(){
	document.getElementById('nome').readOnly = true;
	document.getElementById('terapista').readOnly = true;
	document.getElementById('start').disabled = true;
	document.getElementById('end').disabled = true;
	document.getElementById('stanzaSelect').disabled = true;
	document.getElementById('tariffaSelect').disabled = true;
	document.getElementById('stanzaSelect').disabled = true;
	document.getElementById('lun_start').disabled = true;
	document.getElementById('lun_stop').disabled = true;
	document.getElementById('mar_start').disabled = true;
	document.getElementById('mar_stop').disabled = true;
	document.getElementById('mer_start').disabled = true;
	document.getElementById('mer_stop').disabled = true;
	document.getElementById('gio_start').disabled = true;
	document.getElementById('gio_stop').disabled = true;
	document.getElementById('ven_start').disabled = true;
	document.getElementById('ven_stop').disabled = true;
	document.getElementById('sab_start').disabled = true;
	document.getElementById('sab_stop').disabled = true;
	document.getElementById('dom_start').disabled = true;
	document.getElementById('dom_stop').disabled = true;
	
}

function unLockFields(){
	document.getElementById('nome').readOnly = false;
	document.getElementById('terapista').readOnly = true;
	document.getElementById('start').disabled = false;
	document.getElementById('end').disabled = false;
	document.getElementById('stanzaSelect').disabled = false;
	document.getElementById('tariffaSelect').disabled = false;
	document.getElementById('lun_start').disabled = false;
	document.getElementById('lun_stop').disabled = false;
	document.getElementById('mar_start').disabled = false;
	document.getElementById('mar_stop').disabled = false;
	document.getElementById('mer_start').disabled = false;
	document.getElementById('mer_stop').disabled = false;
	document.getElementById('gio_start').disabled = false;
	document.getElementById('gio_stop').disabled = false;
	document.getElementById('ven_start').disabled = false;
	document.getElementById('ven_stop').disabled = false;
	document.getElementById('sab_start').disabled = false;
	document.getElementById('sab_stop').disabled = false;
	document.getElementById('dom_start').disabled = false;
	document.getElementById('dom_stop').disabled = false;
}

function checkRoleBeforeSave(role){
	// se serve puo essere riattivato per gestire i doposcuola a livello utente
	var scelta = $("#roleSelect").val();
	if(role==="1"){
		return true;
	}else if (role==="2" && scelta==="2"){
		return true;
	}else if (role==="3" && scelta==="3"){
		return true;
	}else{
		//$('#notifica').modal();
		return true;
	}
	
}



function DOPOSCUOLAsalva(){
	
		if(validate()){
			var Query = "INSERT INTO `dopoScuola`(`idTerapista`, `nomeDopoScuola`, `start`, `end`, `lun`, `mar`, `mer`, `gio`, `ven`, `sab`, `dom`, `idTariffa`, `idStanza`) VALUES ("+rowData.ID+", '"+$("#nome").val()+"' , '"+getMysqlDate($("#start").val(),'.')+"', '"+getMysqlDate($("#end").val(),'.')+"', '"+$('#lun_start').val()+"|"+$('#lun_stop').val()+"', '"+$('#mar_start').val()+"|"+$('#mar_stop').val()+"', '"+$('#mer_start').val()+"|"+$('#mer_stop').val()+"', '"+$('#gio_start').val()+"|"+$('#gio_stop').val()+"', '"+$('#ven_start').val()+"|"+$('#ven_stop').val()+"', '"+$('#sab_start').val()+"|"+$('#sab_stop').val()+"', '"+$('#dom_start').val()+"|"+$('#dom_stop').val()+"', "+$("#tariffaSelect").val()+", "+$("#stanzaSelect").val()+")";
			$.ajax({
				  type: 'post',
				  url: '../php/mysql/Query_Insert_Delete.php',
				  data: { 
					'query': Query
				  },
				  success: function (response) {
					creaAppuntamento(JSON.parse(response));
					var Query1 = "INSERT INTO `agenda`(`title`, `start`, `end`, `idTerapista`, `idDopoScuola`, `idTariffa`, `idStanza`, `ricorrenza`, `stato`, `now`) VALUES "+value;
					addAgenda(Query1);
					value="";
					lockFields();
					document.getElementById("floatButtonAdd").style.display = "block";
					document.getElementById("floatButtonSave").style.display = "none";
					document.getElementById("floatButtonEdit").style.display = "block";
					document.getElementById("floatButtonDelete").style.display = "block";
					document.getElementById("floatButtonUpdate").style.display = "none";
					document.getElementById("floatButtonPazNonAss").style.display = "none";
					document.getElementById("floatButtonPazAss").style.display = "block";
					document.getElementById("associaButton").style.display = "none";
					document.getElementById("disassociaButton").style.display = "block";
					
					DOPOSCUOLAinitGrid('SELECT d.ID, t.nomeCognome_t, d.nomeDopoScuola, d.start, d.end, d.lun, d.mar, d.mer, d.gio, d.ven, d.sab, d.dom, d.idTariffa, d.idStanza FROM dopoScuola as d JOIN Terapisti as t ON t.ID = d.idTerapista', 'jqGrid');
				  },
				  error: function () {
					alert("Errore nell'esecuzione della Query! \n"+Query);
				  }
			});	
		}
}

function DOPOSCUOLAupdate(){
	if(validate()){
		var idT = idTerapistaSelected;
		var idD = idDopoScuolaSelected;
		if(idTerapistaSelected !== undefined){
			var Query = "UPDATE `dopoScuola` SET `idTerapista`='"+idTerapistaSelected+"', `nomeDopoScuola`='"+$("#nome").val()+"',`start`= '"+$("#start").val()+"',`end`= '"+$("#end").val()+"' ,`lun`='"+$('#lun_start').val()+"|"+$('#lun_stop').val()+"',`mar`='"+$('#mar_start').val()+"|"+$('#mar_stop').val()+"' ,`mer`='"+$('#mer_start').val()+"|"+$('#mer_stop').val()+"' ,`gio`='"+$('#gio_start').val()+"|"+$('#gio_stop').val()+"' ,`ven`='"+$('#ven_start').val()+"|"+$('#ven_stop').val()+"' ,`sab`='"+$('#sab_start').val()+"|"+$('#sab_stop').val()+"' ,`dom`='"+$('#dom_start').val()+"|"+$('#dom_stop').val()+"' WHERE ID = "+idDopoScuolaSelected;
		}else{
			var Query = "UPDATE `dopoScuola` SET `nomeDopoScuola`='"+$("#nome").val()+"',`start`= '"+$("#start").val()+"',`end`= '"+$("#end").val()+"' ,`lun`='"+$('#lun_start').val()+"|"+$('#lun_stop').val()+"',`mar`='"+$('#mar_start').val()+"|"+$('#mar_stop').val()+"' ,`mer`='"+$('#mer_start').val()+"|"+$('#mer_stop').val()+"' ,`gio`='"+$('#gio_start').val()+"|"+$('#gio_stop').val()+"' ,`ven`='"+$('#ven_start').val()+"|"+$('#ven_stop').val()+"' ,`sab`='"+$('#sab_start').val()+"|"+$('#sab_stop').val()+"' ,`dom`='"+$('#dom_start').val()+"|"+$('#dom_stop').val()+"' WHERE ID = "+idDopoScuolaSelected;
		}
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				
				aggiornaAgendaAppuntamenti();
				
			  },
			  error: function () {
				alert("Errore nell'esecuzione della Query! \n"+Query);
			  }
		});	
	}
}

function aggiornaAgendaAppuntamenti(){
	
		var Query = "UPDATE `agenda` SET `idTerapista`= "+idTerapistaSelected+" WHERE `idDopoScuola`= "+idDopoScuolaSelected;
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
			lockFields();
			document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";
			document.getElementById("floatButtonPazNonAss").style.display = "none";
			document.getElementById("floatButtonPazAss").style.display = "block";
			document.getElementById("associaButton").style.display = "none";
			document.getElementById("disassociaButton").style.display = "block";
			//DOPOSCUOLAinitGrid('SELECT d.ID, t.nomeCognome_t, d.nomeDopoScuola, d.start, d.end, d.lun, d.mar, d.mer, d.gio, d.ven, d.sab, d.dom, d.idTariffa, d.idStanza FROM dopoScuola as d JOIN Terapisti as t ON t.ID = d.idTerapista', 'jqGrid');
			$('#updateDialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}


function addAgenda(Query){
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function DOPOSCUOLAadd(){
	DOPOSCUOLAinitGrid1('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid', true);
}

function DOPOSCUOLAedit(){
	DOPOSCUOLAinitGrid1('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid', false);
}

function DOPOSCUOLAdelete(){
	var Query;
	
	if(idDopoScuolaSelected != undefined){
		Query = "DELETE FROM `dopoScuola` WHERE ID = "+idDopoScuolaSelected;
		
	}else{
		Query = "DELETE FROM `dopoScuola` WHERE nomeDopoScuola = '"+$("#nome").val()+"' AND start = '"+$("#start").val()+"' AND end = '"+$("#end").val()+"'";
	}
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
			$('#dialog').modal();
			DOPOSCUOLAadd()
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
}

function DOPOSCUOLAgetStanze(){
	var Query = "SELECT * FROM Stanza";
	var stanza = document.getElementById("stanzaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			for(var i=0; i<obj.length;i++){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].stanza;
				stanza.add(option);
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Stanze");
		  }
	});

}

function DOPOSCUOLAgetRole(){
	var Query = "SELECT * FROM Role";
	var role = document.getElementById("roleSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			for(var i=0; i<obj.length;i++){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].role;
				role.add(option);
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Ruoli Utente");
		  }
	});

}

function DOPOSCUOLAgetTariffe(){
	$("#tariffaSelect").empty();
	var Query = "SELECT * FROM Tariffa where custom = 0";
	var tariffa = document.getElementById("tariffaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			for(var i=obj.length-1; i>=0;i--){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].tariffa;
				tariffa.add(option);
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Triffe nel Database");
		  }
	});

}

function TERAPISTIgetCategorie(){

	var Query = "SELECT * FROM CategoriaT";
	var categoria = document.getElementById("categoriaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			for(var i=0; i<obj.length;i++){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].tipoCategoria;
				categoria.add(option);
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Categorie nel Database");
		  }
	});

}

function DOPOSCUOLAmostraAlert(){
	if(idDopoScuolaSelected != undefined){
		DOPOSCUOLAdeleteCheck(idDopoScuolaSelected);
	}else{
		$('#attenzione').modal();
	}
}

function validate(){
	
	var	user = $("#nome").val();
	var pwd  = $("#terapista").val();
	var st   = $("#start").val();
	var tar  = $("#end").val();
	
	if(user === "" || user === undefined || pwd === "" || pwd === undefined || st === "" || st === undefined || tar === "" || tar === undefined){
		alert("Attenzione riempire tutti i campi");
		return false;
	}else{
		if($("#lun_start").val() !== "" && $("#lun_stop").val()==="" || $("#lun_stop").val() !== "" && $("#lun_start").val()===""){
			return false;
		}else if($("#lun_start").val() !== "" && $("#lun_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#mar_start").val() !== "" && $("#mar_stop").val()==="" || $("#mar_stop").val() !== "" && $("#mar_start").val()===""){
			return false;
		}else if($("#mar_start").val() !== "" && $("#mar_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#mer_start").val() !== "" && $("#mer_stop").val()==="" || $("#mer_stop").val() !== "" && $("#mer_start").val()===""){
			return false;
		}else if($("#mer_start").val() !== "" && $("#mer_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#gio_start").val() !== "" && $("#gio_stop").val()==="" || $("#gio_stop").val() !== "" && $("#gio_start").val()===""){
			return false;
		}else if($("#gio_start").val() !== "" && $("#gio_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#ven_start").val() !== "" && $("#ven_stop").val()==="" || $("#ven_stop").val() !== "" && $("#ven_start").val()===""){
			return false;
		}else if($("#ven_start").val() !== "" && $("#ven_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#sab_start").val() !== "" && $("#sab_stop").val()==="" || $("#sab_stop").val() !== "" && $("#sab_start").val()===""){
			return false;
		}else if($("#sab_start").val() !== "" && $("#sab_stop").val()!==""){
			allEmpty = false;
		}
		
		if($("#dom_start").val() !== "" && $("#dom_stop").val()==="" || $("#dom_stop").val() !== "" && $("#dom_start").val()===""){
			return false;
		}else if($("#dom_start").val() !== "" && $("#dom_stop").val()!==""){
			allEmpty = false;
		}
		
		return true;
	}
	
}

function deleteAgenda(ID){
	Query = "UPDATE `agenda` SET eventoCancellato = 1 WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			return true;
		  },
		  error: function () {
			return false;
		  }
	});	
	
}

function deleteJoin(ID){
	Query = "DELETE FROM `joinPazienteTariffaTerapista` WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function DOPOSCUOLAdeleteCheck(ID){
	Query = "SELECT * FROM `joinPazienteDopoScuola` WHERE `idDopoScuola` = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  obj = JSON.parse(response);
			if(obj.length > 0){  
				$('#noDelete').modal();
			}else{
				DOPOSCUOLAdelete();
			}
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
}

function updateAgenda(ID , NewID){
	Query = "UPDATE `agenda` SET idTerapista = "+NewID+" WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			return true;
		  },
		  error: function () {
			return false;
		  }
	});	
	
}

function updateJoin(ID, NewID){
	Query = "UPDATE `joinPazienteTariffaTerapista` SET idTerapista = "+NewID+" WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function showTerapisti(){
	document.getElementById("associa2").style.display = "block";
	document.getElementById("associa1").style.display = "none";
	document.getElementById("terapisti").style.display = "block";
	DOPOSCUOLAinitGrid1("Select * from Terapisti WHERE NOT ID = "+rowData.ID+" AND NOT ID = 1", 'jqGrid1');
	
	
}

function addUserLogin(user, pasword){
	
	Query = "CREATE USER '"+user+"'@'%' IDENTIFIED BY '"+pasword+"'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			grantUser(user);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
	
}

function deleteUserLogin(){
	Query = "DROP USER '"+user+"'@'%'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
}

function creaAppuntamento(response){
	var now = moment().valueOf();
	if($('#lun_start').val() !== undefined || $('#lun_stop').val() !== undefined){
		lun = $('#lun_start').val()+"|"+$('#lun_stop').val();
		if(lun!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#lun_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#lun_stop').val(),1);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#lun_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#lun_stop').val(),1);
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#mar_start').val() !== undefined || $('#mar_stop').val() !== undefined){
		mar = $('#mar_start').val()+"|"+$('#mar_stop').val();
		if(mar!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#mar_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#mar_stop').val(),2);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#mar_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#mar_stop').val(),2);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#mer_start').val() !== undefined || $('#mer_stop').val() !== undefined){
		mer = $('#mer_start').val()+"|"+$('#mer_stop').val();
		if(mer!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#mer_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#mer_stop').val(),3);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#mer_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#mer_stop').val(),3);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#gio_start').val() !== undefined || $('#gio_stop').val() !== undefined){
		gio = $('#gio_start').val()+"|"+$('#gio_stop').val();
		if(gio!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#gio_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#gio_stop').val(),4);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#gio_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#gio_stop').val(),4);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#ven_start').val() !== undefined || $('#ven_stop').val() !== undefined){
		ven = $('#ven_start').val()+"|"+$('#ven_stop').val();
		if(ven!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#ven_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#ven_stop').val(),5);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#ven_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#ven_stop').val(),5);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#sab_start').val() !== undefined || $('#sab_stop').val() !== undefined){
		sab = $('#sab_start').val()+"|"+$('#sab_stop').val();
		if(sab!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#sab_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#sab_stop').val(),6);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#sab_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#sab_stop').val(),6);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
	if($('#dom_start').val() !== undefined || $('#dom_stop').val() !== undefined){
		dom = $('#dom_start').val()+"|"+$('#dom_stop').val();
		if(dom!=="|"){
			var start = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#dom_start').val(),getMysqlDate($("#end").val(),'.')+" "+$('#dom_stop').val(),7);
			var end = getAgendaInsertQueryStart(getMysqlDate($("#start").val(),'.')+" "+$('#dom_stop').val(),getMysqlDate($("#end").val(),'.')+" "+$('#dom_stop').val(),7);
			if(value !== ""){
				value += ","
			}
			for(var i = 0; i < start.length; i++){		
				if(i=== start.length-1){
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"', '"+"0"+"', '"+now+"')";
				}else{
					value += "('"+$('#nome').val()+"', '"+start[i]+"', '"+end[i]+"', "+rowData.ID+", '"+response[0].id+"', '"+"1"+"', '"+"1"+"', '"+"Settimanale"+"','"+"0"+"', '"+now+"'),";
				}
			}
		}
	}
}
//dom = 0, lun = 1, mar = 3, mer = 4, gio = 5, ven = 6, sab = 7
function getAgendaInsertQueryStart(start, end, day){
	var inizio = moment(start), // Sept. 1st
    fine   = moment(end),
	giorno = day;	// Nov. 2nd
    

	var result = [];
	var current = inizio.clone();

	while (current.day(7 + day).isBefore(fine)) {
	  result.push(current.clone());
	}
	console.log(result.map(m => m.format('YYYY-MM-DD HH:mm')));
	return result.map(m => m.format('YYYY-MM-DD HH:mm'));
	
}
function getAgendaInsertQueryEnd(start, end, day){
	var inizio = moment(start), // Sept. 1st
    fine   = moment(end),
	giorno = day;	// Nov. 2nd
    

	var result = [];
	var current = inizio.clone();

	while (current.day(7 + day).isBefore(fine)) {
	  result.push(current.clone());
	}
	console.log(result.map(m => m.format('YYYY-MM-DD HH:mm')));
	return result;
	
}

function grantUser(user){
	
	Query = "GRANT ALL PRIVILEGES ON * . * TO '"+user+"'@'%'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function DOPOSCUOLAinitGridPazAzz(Query, GridName, w){
	obj="";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			showGridPazAss(obj, GridName, w);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function showGridPazAss(obj, GridName, w) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome", label: "Nome Paziente:" },
			{ name: 'seleziona', width: 80, align: "center", editoptions: { value: "True:False" }, editrules: { required: true }, formatter: "checkbox", formatoptions: { disabled: false }, editable: true  } 
        ],
		width: w,
		autowidth: false,
		shrinkToFit: true,
		rowNum: 12,
        pager: "#jqGridPagerPazAss",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeCognome",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        beforeSelectRow: function (rowid, e) {
			$self = $(this),
            iCol = $.jgrid.getCellIndex($(e.target).closest("td")[0]),
            cm = $self.jqGrid("getGridParam", "colModel"),
            localData = $self.jqGrid("getLocalRow", rowid);
        if (cm[iCol].name === "seleziona" && e.target.tagName.toUpperCase() === "INPUT") {
            // set local grid data
            localData.seleziona = $(e.target).is(":checked");
			if(localData.seleziona){
				daDisassociare.set(localData.idPaziente, localData.idPaziente);
			}else{
				daDisassociare.delete(localData.idPaziente);
			}
            
        }
        
        return true; // allow selection
		},
    });
    $grid.jqGrid("navGrid", "#jqGridPagerPazAss",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

function DOPOSCUOLAmostraPazientiAssociati(){
	document.getElementById("associaButton").style.display = "none";
	document.getElementById("disassociaButton").style.display = "block";
	$('#assPaz').modal();
	$('#assPaz').on('shown.bs.modal', function (e) {
		daDisassociare.clear();
		DOPOSCUOLAinitGridPazAzz("SELECT p.nomeCognome, p.idPaziente from joinPazienteDopoScuola as j JOIN paziente AS p ON p.idPaziente = j.idPaziente WHERE idDopoScuola = "+idDopoScuolaSelected, "jqGridPazAss",$("#larg").innerWidth()-40);
	});
}

function DOPOSCUOLADisassociaPazienti(){
	var valore="";
	for (var [key, value] of daDisassociare) {
		valore+=value+",";
		console.log(key + " = " + value);
	}
	var Query = "DELETE from joinPazienteDopoScuola WHERE idPaziente IN ("+valore.substring(0,valore.length-1)+") AND idDopoScuola = "+idDopoScuolaSelected;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function DOPOSCUOLAAssociaPazienti(){
	var valore="";
	for (var [key, value] of daDisassociare) {
		valore+=("("+value+","+idDopoScuolaSelected+",0),");
		console.log(key + " = " + value);
	}
	valore = valore.substring(0,valore.length-1)+";";
	var Query = "INSERT INTO joinPazienteDopoScuola (IdPaziente, idDopoScuola, presenza) value "+valore+"";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}
function DOPOSCUOLAmostraPazientiNonAssociati(){
	
	$('#assPaz').modal();
	$('#assPaz').on('shown.bs.modal', function (e) {
		daDisassociare.clear();
		DOPOSCUOLAinitGridPazAzz("SELECT p.idPaziente, p.nomeCognome FROM paziente AS p WHERE idPaziente NOT IN (SELECT joinPazienteDopoScuola.idPaziente from joinPazienteDopoScuola WHERE idDopoScuola = "+idDopoScuolaSelected+")", "jqGridPazAss",$("#larg").innerWidth()-40)
	});
	
}

function addTariffa(){
	
	if($('#tariffaDD').val().length==0){
	
	}else{

		var Query="INSERT INTO `Tariffa` (`tariffa`, `custom`) VALUES ('"+$("#tariffaDD").val()+"',0)";
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				
				$('#tariffaModal').modal('toggle');
				DOPOSCUOLAgetTariffe();  
			  },
			  error: function () {
				alert("error");
			  }
			});	
	}	
}
