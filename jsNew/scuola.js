
function initGridPresenze(idDopoScuola,w){
	
	var Query_presenze =  'SELECT nomeCognome,a.IdPaziente,presenza,confermato,idDopoScuola FROM joinPazienteDopoScuola as a JOIN paziente as p ON a.IdPaziente = p.idPaziente JOIN dopoScuola as dS ON a.idDopoScuola = dS.ID WHERE idDopoScuola='+idDopoScuola+' ORDER BY nomeCognome';
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query_presenze
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			//console.log(obj);
			gridPresenze(obj,w);
		  },
		  error: function () {
			alert("error");
		  }
	});	
}

function checkConferma(index){
	if (index=="1"){
		return {disabled: true}
	} else {
		return {disabled: false};
	}
}


function gridPresenze(obj,w) {
	$grid = $("#gridPresenze");
	//$grid = $("#"+GridName+"");
	//$(GridName).jqGrid('GridUnload');
	$grid.jqGrid('GridUnload');
	var conferma = obj[0].confermato;
    $grid.jqGrid({
        data: obj,
        datatype: "local",
		colNames:["Paziente", "Presenza"],
        colModel: [
            { name: "nomeCognome", width:350 },
			{
				name:"presenza",  
				width:100,
				editable: false,        
				sortable: false, 
				align: "center", 
				formatter: "checkbox", 
				editoptions: {value: "1:0"},
				formatoptions: checkConferma(conferma)
				
				/*  function() {
						for(var i=0;i<obj.length;i++){
						if(obj[i].confermato=="0"){
						{disabled: false}
						} else {
						{disabled: true}
						}
						}
				} */
			} 
            
        ],
		//autowidth: true,
		width: w,
		shrinkToFit: true,
        pager: "#packagePager",
        rowNum: 10,
        rowList: [1, 2, 10],
		caption: "cerca",
        viewrecords: true,
		height: "auto",
        sortname: "nomeCognome",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
		}
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
	
	//$('#inserisciPresenze').modal();
	
}

function SCUOLAcancellaPresenze(){
	var rowData = $("#gridPresenze").jqGrid("getGridParam", "data");
	
	var ids = $("#gridPresenze").jqGrid('getDataIDs'); 
	for (var i = 0; i < ids.length; i++) { 
	  $("#gridPresenze").saveRow(ids[i], false); 
	  rowData[i].presenza = $("#gridPresenze").jqGrid("getRowData", ids[i]).presenza;
	}
	
	
	var query_cancella_presenze = "DELETE FROM `joinPazienteDopoScuola` WHERE idDopoScuola="+rowData[0].idDopoScuola+"";
	
		$.ajax({

			type: 'post',
			url: '../php/mysql/Query_Insert_Delete.php',
			data: {
				'query': query_cancella_presenze
				},
			success: function (response) {
			inserisciPresenze();
			},

			error: function () {
				alert("error");
			}
		}); 
}

function inserisciPresenze(){
	var valori = "";
	var rowData = $("#gridPresenze").jqGrid("getGridParam", "data");
	
	var query_aggiorna_presenze_parziale = "INSERT INTO `joinPazienteDopoScuola`(`IdPaziente`, `idDopoScuola`, `presenza`) VALUES ";

	for(var i=0;i<rowData.length;i++){
		
		if(i+1==rowData.length) {
			valori = valori + "("+rowData[i].IdPaziente + ","+rowData[i].idDopoScuola + ","+rowData[i].presenza+")"
		} else {
			valori = valori + "("+rowData[i].IdPaziente + ","+rowData[i].idDopoScuola + ","+rowData[i].presenza+"),"
		}
	}
	
	var query_aggiorna_presenze = query_aggiorna_presenze_parziale + valori;
	
	$.ajax({

			type: 'post',
			url: '../php/mysql/Query_Insert_Delete.php',
			data: {
				'query': query_aggiorna_presenze
				},
			success: function (response) {
			window.parent.document.getElementById('myBtn').click(); 
			$('#presenzeInserite').modal();
			},

			error: function () {
				alert("error");
			}
		});
}


function mostra_menu_scuola_admin(idDopoScuola,confermato){
	$.contextMenu('destroy');
	
	$(function(){
    $.contextMenu({
        selector: '#calendar', 
		events:{
            hide:function () {
                $.contextMenu('destroy');
		}},
		trigger: 'left',
		hideOnSecondTrigger: true,
		callback: function(key, options) {
            if (key=="show"){
				
				$('#TitoloAppuntamentoDS').html(datiAppuntamento.title);
				$('#NoteAppuntamentoDS').val(datiAppuntamento.nota);
				$('#DataAppuntamentoDS').val(convertiData(datiAppuntamento.start,"anno",4));
				$('#OraStartAppuntamentoDS').val(convertiData(datiAppuntamento.start,"ora",4));
				$('#OraEndAppuntamentoDS').val(convertiData(datiAppuntamento.end,"ora",4));
				window.parent.document.getElementById('myBtn').click(); 
				$('#mostraEventoDopoScuola').modal();
				
			} else if (key=="inserisci") {
				$('#inserisciPresenze').modal();
				$('#inserisciPresenze').on('shown.bs.modal', function (e) { 
				 initGridPresenze(idDopoScuola,$("#larg").innerWidth()-60);
				  });
				//initGridPresenze(idDopoScuola);
			} else if (key=="blocca") {
				
				bloccaPresenze(idDopoScuola);
			} else if (key=="sblocca") {
				
				sbloccaPresenze(idDopoScuola);
			}
        },
        items: {
					"show": {name: "Visualizza", icon: "fa-eye"},
					//prova menu grip doposcuola
					"inserisci": {name: "Inserisci presenze", icon: "fa-check-circle", disabled: function(key, opt) {
									if(confermato==1){
										return true;
									}else{
										return false;
									}
									}},
					"blocca": {name: "Blocca presenze", icon: "fa-lock", disabled: function(key, opt) {
									if(confermato==1){
										return true;
									}else{
										return false;
									}
									}},
					"sblocca": {name: "Sblocca presenze", icon: "fa-unlock", disabled: function(key, opt) {
									if(confermato==1){
										return false;
									}else{
										return true;
									}
									}},
					"sep1": "---------",
					"quit": {name: "Chiudi", icon: "fa-times"}
        }
    });
	});
	

	
}	

function mostra_menu_scuola_user(idDopoScuola,confermato){
	$.contextMenu('destroy');
	
	$(function(){
    $.contextMenu({
        selector: '#calendar', 
		events:{
            hide:function () {
                $.contextMenu('destroy');
		}},
		trigger: 'left',
		hideOnSecondTrigger: true,
		callback: function(key, options) {
               if (key=="show"){
				
				$('#TitoloAppuntamentoDS').html(datiAppuntamento.title);
				$('#NoteAppuntamentoDS').val(datiAppuntamento.nota);
				$('#DataAppuntamentoDS').val(convertiData(datiAppuntamento.start,"anno",4));
				$('#OraStartAppuntamentoDS').val(convertiData(datiAppuntamento.start,"ora",4));
				$('#OraEndAppuntamentoDS').val(convertiData(datiAppuntamento.end,"ora",4));
				
				window.parent.document.getElementById('myBtn').click(); 
				$('#mostraEventoDopoScuola').modal();
			}  else if (key=="delete2"){
				$('#EliminaRicorrenza').modal();
			}  else if (key=="inserisci") {
				
				$('#inserisciPresenze').modal();
				$('#inserisciPresenze').on('shown.bs.modal', function (e) { 
				 initGridPresenze(idDopoScuola,$("#larg").innerWidth()-60);
				  });
			} else if (key=="blocca") {
				
				bloccaPresenze(idDopoScuola);
			}
        },
        items: {
					"show": {name: "Visualizza", icon: "fa-eye"},
					//prova menu grip doposcuola
					"inserisci": {name: "Inserisci presenze", icon: "fa-check-circle", disabled: function(key, opt) {
									if(confermato==1){
										return true;
									}else{
										return false;
									}
									}},
					"blocca": {name: "Blocca presenze", icon: "fa-lock", disabled: function(key, opt) {
									if(confermato!=1){
										return false;
									}else{
										return true;
									}
									}},
					"sep1": "---------",
					"quit": {name: "Chiudi", icon: "fa-times"}
        }
    });
	});
	

	
}	

function bloccaPresenze(idDopoScuola){
	
	
	var query_blocca_presenze = "UPDATE `dopoScuola` SET `confermato`= 1 WHERE ID= "+idDopoScuola+"";

	$.ajax({

			type: 'post',
			url: '../php/mysql/Query_Insert_Delete.php',
			data: {
				'query': query_blocca_presenze
				},
			success: function (response) {
			location.reload();
			},

			error: function () {
				alert("error");
			}
		});
}

function sbloccaPresenze(idDopoScuola){
	
	
	var query_blocca_presenze = "UPDATE `dopoScuola` SET `confermato`= 0 WHERE ID= "+idDopoScuola+"";

	$.ajax({

			type: 'post',
			url: '../php/mysql/Query_Insert_Delete.php',
			data: {
				'query': query_blocca_presenze
				},
			success: function (response) {
			location.reload();
			},

			error: function () {
				alert("error");
			}
		});
}

