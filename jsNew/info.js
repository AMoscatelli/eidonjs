var licence;
var owner;
var activationDate;
var expirationDate;
var mostrata;


function getInfo() {
	
		var query_info = 'SELECT * FROM `info`';
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_info
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			
				licence = obj[0].licenceNumber;
				owner = obj[0].owner;
				activationDate = obj[0].activationDate;
				expirationDate = obj[0].expirationDate;
				mostrata = obj[0].mostrato;
				calcolaGiorni(expirationDate);
				
		},
		  error: function () {
			alert("error");
		  }
	});
	
	
	
}

var intFin;

function mostraOK(){
	
	var query_ok = 'UPDATE `info` SET `mostrato`=1 WHERE ID=1';
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': query_ok
		  },
		  success: function (response) {
				
		},
		  error: function () {
			alert("error");
		  }
	});
	
}

function mostraKO(){
	
	var query_ko = 'UPDATE `info` SET `mostrato`=0 WHERE ID=1';
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': query_ko
		  },
		  success: function (response) {
				
		},
		  error: function () {
			alert("error");
		  }
	});
	
}

function calcolaGiorni(data){
	
	var dataInt2 = parseInt(Date.parse(new Date(data+"T00:00:00")));
	
	var now = new Date().getTime();
	
	intFin = parseInt((dataInt2 - now)/86400000);
	
	if (intFin<30){
		
		if(mostrata==1){
			
		} else {
		
		$('#licenzaInScadenza').modal();
		
		mostraOK();
		
		}
	
	}
	
	mostraInfo();
	
}

function checkMostrata(){
	return mostrata;
}
function mostraInfo(){
	
	$('#licenza').val(licence);
	$('#owner').val(owner);
	$('#activationData').val(activationDate);
	$('#expirationData').val(expirationDate);
	$('#days').val(intFin);
	
}
