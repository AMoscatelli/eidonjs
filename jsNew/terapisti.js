var obj = [];
var rowData;
var p;
function TERAPISTIinitGrid(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			showGrid(obj, GridName);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function TERAPISTIinitGrid1(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {

			obj = JSON.parse(response);
			showGrid1(obj, GridName);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}

function showGrid1(obj, GridName) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome_t", label: "Cognome Nome:", width: 110 }
            
        ],
		autowidth: true,
		rowNum: 12,
        pager: "#jqGridPager1",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeCognome_t",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData1 = $(this).jqGrid("getLocalRow", rowid), str = "", p;
            for (p in rowData1) {
                if (rowData1.hasOwnProperty(p)) {
                    str += "propery \"" + p + "\" + have the value \"" + rowData1[p] + "\n";
                }
            }
			NewID = rowData1.ID;
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

function showGrid(obj, GridName) {
	
	$grid = $("#"+GridName);
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome_t", label: "Cognome Nome:", width: 110 }
            
        ],
		autowidth: true,
		rowNum: 12,
        pager: "#jqGridPager",
        viewrecords: true,
        rownumbers: false,
        caption: "Cerca:",
        height: "auto",
        sortname: "nomeCognome_t",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
            for (p in rowData) {
                if (rowData.hasOwnProperty(p)) {
                    str += "propery \"" + p + "\" + have the value \"" + rowData[p] + "\n";
                }
            }
			$("#nome").focus();
			$("#nome").val(rowData.nomeCognome_t);
			$("#user").val(rowData.user);
			$("#password").val("xxxxxxxxxxxxxxxxxx");
			$("#stanzaSelect").val(rowData.idStanza);
			$("#tariffaSelect").val(rowData.idTariffa);
			$("#categoriaSelect").val(rowData.idCategoria);
			$("#categoriaSelectOpt").val(rowData.idCategoria1 );
			$("#coloreSelect").val(rowData.color);
			$("#roleSelect").val(rowData.role);
			if(rowData.ritAcconto == 1){
				$("#ritAcconto").prop('checked', true);
			}else{
				$("#ritAcconto").prop('checked', false);
			}
			lockFields();
			document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";
 
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}
	
function lockFields(){
	document.getElementById('nome').readOnly = true;
	document.getElementById('user').readOnly = true;
	document.getElementById('password').readOnly = true;
	document.getElementById('stanzaSelect').disabled = true;
	document.getElementById('tariffaSelect').disabled = true;
	document.getElementById('coloreSelect').disabled = true;
	document.getElementById('categoriaSelect').disabled = true;
	document.getElementById('categoriaSelectOpt').disabled = true;
	document.getElementById('roleSelect').disabled = true;
}

function unLockFields(){
	document.getElementById('nome').readOnly = false;
	document.getElementById('user').readOnly = false;
	document.getElementById('password').readOnly = false;
	document.getElementById('stanzaSelect').disabled = false;
	document.getElementById('tariffaSelect').disabled = false;
	document.getElementById('coloreSelect').disabled = false;
	document.getElementById('categoriaSelect').disabled = false;
	document.getElementById('categoriaSelectOpt').disabled = false;
}

function checkRoleBeforeSave(role){
	var scelta = $("#roleSelect").val();
	if(role==="1"){
		return true;
	}else if (role==="2" && scelta==="2"){
		return true;
	}else if (role==="3" && scelta==="3"){
		return true;
	}else{
		$('#notifica').modal();
		return false;
	}
	
}

function TERAPISTIsalva(role){
	var role = checkCookie();
	var socio = false;
	var idStanza;
	var idTariffa;
	if(checkRoleBeforeSave(role[0])){
		if(validate()){
			if($('#ritAcconto:checked').val() === "on"){
				var ritAcc = 1;
			}else{
				var ritAcc = 0;
			}
			var Query = "INSERT INTO `Terapisti`(`nomeCognome_t`, `idStanza`, `idTariffa`, `color`, `user`, `pwd`,`role`, `idCategoria`, `idCategoria1`, `ritAcconto` ) VALUES ('"+$("#nome").val()+"', "+$("#stanzaSelect").val()+", "+$("#tariffaSelect").val()+", '"+$("#coloreSelect").val()+"', '"+$("#user").val()+"', md5('"+$("#password").val()+"'), "+$("#roleSelect").val()+", "+$("#categoriaSelect").val()+", "+$("#categoriaSelectOpt").val()+", "+ritAcc+")";
			if($("#categoriaSelect").val()==1 || $("#categoriaSelectOpt").val()==1){
				socio = true;
				idStanza=$("#stanzaSelect").val();
				idTariffa=$("#tariffaSelect").val();	
			}
			$.ajax({
				  type: 'post',
				  url: '../php/mysql/Query_Insert_Delete.php',
				  data: { 
					'query': Query
				  },
				  success: function (response) {
					  if(socio){
						  objID=JSON.parse(response);
							idTerapista=objID[0].id;
						  addJoinPazienteDefult(idTerapista, idStanza, idTariffa);
					  }
					TERAPISTIinitGrid('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid');
					window.parent.scrollTo(0, 0);
					$('#dialog').modal();
					lockFields();
					document.getElementById("floatButtonAdd").style.display = "block";
					document.getElementById("floatButtonSave").style.display = "none";
					document.getElementById("floatButtonEdit").style.display = "block";
					document.getElementById("floatButtonDelete").style.display = "block";
					document.getElementById("floatButtonUpdate").style.display = "none";
				  },
				  error: function () {
					alert("Errore nell'esecuzione della Query! \n"+Query);
				  }
			});	
		}
	}
}

function  addJoinPazienteDefult(idTerapista, idStanza, idTariffa){
	
	
	var Query = "INSERT INTO `joinpazientetariffaterapista`(`idPaziente`, `idTariffa`, `idTerapista`, `idStanza`) VALUES (999999,"+idTariffa+","+idTerapista+","+idStanza+")";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function TERAPISTIupdate(){
	if(validate()){
		if($('#ritAcconto:checked').val() === "on"){
			var ritAcc = true;
		}else{
			var ritAcc = false;
		}
		var Query = "UPDATE `Terapisti` SET `user`='"+$("#user").val()+"', `nomeCognome_t`='"+$("#nome").val()+"', `idStanza`="+$("#stanzaSelect").val()+",`idTariffa`="+$("#tariffaSelect").val()+",`color`='"+$("#coloreSelect").val()+"',`idCategoria`="+$("#categoriaSelect").val()+",`idCategoria1`="+$("#categoriaSelectOpt").val()+" ,`ritAcconto`="+ritAcc+" WHERE ID = "+rowData.ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
			TERAPISTIinitGrid('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid');
			TERAPISTIupdateJoin();
			lockFields();
			document.getElementById("floatButtonAdd").style.display = "block";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonEdit").style.display = "block";
			document.getElementById("floatButtonDelete").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	}
}

function TERAPISTIupdateJoin(){
	if(validate()){
	var Query = "UPDATE `joinPazienteTariffaTerapista` SET `idStanza`="+$("#stanzaSelect").val()+",`idTariffa`="+$("#tariffaSelect").val()+" WHERE idTerapista = "+rowData.ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	}
}

function TERAPISTIadd(){
			unLockFields();
			$("#nome").focus();
			$("#nome").val("");
			$("#user").val("");
			$("#password").val("");
			$("#stanzaSelect").val("");
			$("#tariffaSelect").val("");
			$("#categoriaSelect").val("");
			$("#categoriaSelectOpt").val("");
			$("#coloreSelect").val("");
			document.getElementById("floatButtonEdit").style.display = "none";
			document.getElementById("floatButtonAdd").style.display = "none";
			document.getElementById("floatButtonDelete").style.display = "none";
			document.getElementById("floatButtonSave").style.display = "block";
			document.getElementById("floatButtonUpdate").style.display = "none";

}

function TERAPISTIedit(){
			unLockFields();
			$("#nome").focus();
			document.getElementById("floatButtonEdit").style.display = "none";
			document.getElementById("floatButtonAdd").style.display = "none";
			document.getElementById("floatButtonDelete").style.display = "none";
			document.getElementById("floatButtonSave").style.display = "none";
			document.getElementById("floatButtonUpdate").style.display = "block";

}

function TERAPISTIdelete(){
	var Query;
	
	if(rowData != undefined){
		Query = "DELETE FROM `Terapisti` WHERE ID = "+rowData.ID;
		
	}else{
		Query = "DELETE FROM `Terapisti` WHERE nomeCognome_t = '"+$("#nome").val()+"' AND idStanza = "+$("#stanzaSelect").val()+" AND idTariffa = "+$("#tariffaSelect").val()+" AND color = '"+$("#coloreSelect").val()+"'";
	}
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
			TERAPISTIinitGrid('Select * from Terapisti WHERE NOT ID = 1', 'jqGrid');
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
			TERAPISTIadd()
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
}

function TERAPISTIgetStanze(){
	$("#stanzaSelect").empty();
	var Query = "SELECT * FROM Stanza";
	var stanza = document.getElementById("stanzaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			if(obj.length>0){
//				var option = document.createElement("option");
//				option.value = 9999;
//				option.text = "Nessuna";
//				stanza.add(option);
				for(var i=obj.length-1; i>=0;i--){
					var option = document.createElement("option");
					option.value = obj[i].ID;
					option.text = obj[i].stanza;
					stanza.add(option);
				}
			}else{
				window.parent.scrollTo(0, 0);
				$('#messaggio').html("Non sono presenti Stanze.\n Crea prima una stanza.");
				$('.associa1').html("Crea Stanza");
				$('.associa1').attr('onclick', 'MENUcallMenuItem1("utility/utility_stanza.html")')
				$('#introModal').modal();
				
				
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Stanze");
		  }
	});

}

function TERAPISTIgetRole(){
	var Query = "SELECT * FROM Role";
	var role = document.getElementById("roleSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			if(obj.length>0){
			for(var i=0; i<obj.length;i++){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].role;
				role.add(option);
			}
			}else{
				window.parent.scrollTo(0, 0);
				$('#introModal').modal();
			}
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Ruoli Utente");
		  }
	});

}

function TERAPISTIgetTariffe(){
	$("#tariffaSelect").empty();
	var Query = "SELECT * FROM Tariffa WHERE NOT custom = 1";
	var tariffa = document.getElementById("tariffaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			if(obj.length>0){
			for(var i=obj.length-1; i>=0;i--){
				var option = document.createElement("option");
				option.value = obj[i].ID;
				option.text = obj[i].tariffa;
				tariffa.add(option);
			}
			}else{
				window.parent.scrollTo(0, 0);
				$('#messaggio').html("Non sono presenti Tariffe,\n Crea prima una tariffa.");
				$('.associa1').html("Crea Tariffa");
				$('.associa1').attr('onclick', 'MENUcallMenuItem1("utility/utility_tariffa.html")')
				$('#introModal').modal();
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Triffe nel Database");
		  }
	});

}

function TERAPISTIgetCategorie(){
	$("#categoriaSelect").empty();
	$("#categoriaSelectOpt").empty();
	var Query = "SELECT * FROM CategoriaT";
	var categoria = document.getElementById("categoriaSelect");
	var categoriaOpt = document.getElementById("categoriaSelectOpt");
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			if(obj.length>0){
				
				var option = document.createElement("option");
				var option1 = document.createElement("option");
				
				option.value = 9999;
				option.text = "Nessuna";
				
				option1.value = 9999;
				option1.text = "Nessuna";
				
				categoria.add(option);
				categoriaOpt.add(option1);
				
			for(var i=obj.length-1; i>=0;i--){
				var option = document.createElement("option");
				var option1 = document.createElement("option");
				
				option.value = obj[i].ID;
				option.text = obj[i].tipoCategoria;
				
				option1.value = obj[i].ID;
				option1.text = obj[i].tipoCategoria;
				
				categoria.add(option);
				categoriaOpt.add(option1);
			}
			}else{
				window.parent.scrollTo(0, 0);
				$('#messaggio').html("Non sono presenti Categorie,\n Crea prima una categoria.");
				$('.associa1').html("Crea Categoria");
				$('.associa1').attr('onclick', 'MENUcallMenuItem1("utility/utility_categoriaT.html")')
				$('#introModal').modal();
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Categorie nel Database");
		  }
	});

}

function TERAPISTImostraAlert(){
	if(rowData != undefined){
		TERAPISTIdeleteCheck(rowData.ID);
	}else{
		window.parent.scrollTo(0, 0);
		$('#attenzione').modal();
	}
}

function validate(){
	
	var	user =	$("#user").val();
	var pwd = $("#password").val();
//	var st = $("#stanzaSelect").val();
	var tar = $("#tariffaSelect").val();
	var col = $("#coloreSelect").val();
	var cat = $("#coloreSelect").val();
	var nome = $("#nome").val();
	 if(nome=="" || nome==undefined || user === "" || user === undefined || pwd === "" || pwd === undefined || tar === "" || tar === undefined || col === "" || col === undefined || cat === "" || cat === undefined){
		window.parent.scrollTo(0, 0);
		$('#introModal').modal();
		return false;
	 }else{
		return true;
	 }
	
}

//quando cancello un terapista devo poter passare tutti i suoi appuntamenti a un nuovo terapista...

function deleteAgenda(ID){
	Query = "UPDATE `agenda` SET eventoCancellato = 1 WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			return true;
		  },
		  error: function () {
			return false;
		  }
	});	
	
}

function deleteJoin(ID){
	Query = "DELETE FROM `joinPazienteTariffaTerapista` WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function TERAPISTIdeleteCheck(ID){
	Query = "SELECT * FROM `joinPazienteTariffaTerapista` WHERE `idTerapista` = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  obj = JSON.parse(response);
			if(obj.length > 0){  
			    window.parent.scrollTo(0, 0);
				$('#changeTerapista').modal();
				TERAPISTIinitGrid("Select * from Terapisti WHERE NOT ID = "+rowData.ID, 'jqGrid1');
			}else{
				TERAPISTIdelete();
			}
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
}

function TERAPISTIdeleteAll(){
	deleteAgenda(rowData.ID);
	deleteJoin(rowData.ID);
	TERAPISTIdelete();
	window.parent.scrollTo(0, 0);
	$('#dialog').modal();
}

function updateAgenda(ID , NewID){
	Query = "UPDATE `agenda` SET idTerapista = "+NewID+" WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			return true;
		  },
		  error: function () {
			return false;
		  }
	});	
	
}

function updateJoin(ID, NewID){
	Query = "UPDATE `joinPazienteTariffaTerapista` SET idTerapista = "+NewID+" WHERE idTerapista = "+ID;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function showTerapisti(){
	document.getElementById("associa2").style.display = "block";
	document.getElementById("associa1").style.display = "none";
	document.getElementById("terapisti").style.display = "block";
	TERAPISTIinitGrid1("Select * from Terapisti WHERE NOT ID = "+rowData.ID+" AND NOT ID = 1", 'jqGrid1');
	
	
}

function TERAPISTIupdateAll(){
	updateAgenda(rowData.ID, NewID);
	updateJoin(rowData.ID, NewID);
	TERAPISTIdelete();
	window.parent.scrollTo(0, 0);
	$('#dialog').modal();
}

function addUserLogin(user, pasword){
	
	Query = "CREATE USER '"+user+"'@'%' IDENTIFIED BY '"+pasword+"'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			grantUser(user);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
	
}

function deleteUserLogin(){
	Query = "DROP USER '"+user+"'@'%'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
}

function grantUser(user){
	Query = "GRANT ALL PRIVILEGES ON * . * TO '"+user+"'@'%'";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#dialog').modal();
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});	
	
}

function addCategoriaT(){
	if($('#fisso:checked').val() === "on"){
		percentuale=0;
	}else{
		percentuale=1;
	} 

	var Query="INSERT INTO `CategoriaT` (`tipoCategoria`, `quota`, `percentuale`) VALUES ('"+$("#nomeCat").val()+"','"+$("#quota").val()+"',"+percentuale+")";
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			window.parent.scrollTo(0, 0);
			$('#categorieModal').modal('toggle');
			TERAPISTIgetCategorie();  
		  },
		  error: function () {
			alert("error");
		  }
		});	
}
function addStanza(){
	if($('#stanzaDD').val().length==0){
	
	}else{

		var Query="INSERT INTO `Stanza` (`stanza`) VALUES ('"+$("#stanzaDD").val()+"')";
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				window.parent.scrollTo(0, 0);
				$('#stanzaModal').modal('toggle');
				TERAPISTIgetStanze();  
			  },
			  error: function () {
				alert("error");
			  }
			});	
	}
}
function addTariffa(){
	
	if($('#tariffaDD').val().length==0){
	
	}else{

		var Query="INSERT INTO `Tariffa` (`tariffa`, `custom`) VALUES ('"+$("#tariffaDD").val()+"',0)";
		$.ajax({
			  type: 'post',
			  url: '../php/mysql/Query_Insert_Delete.php',
			  data: { 
				'query': Query
			  },
			  success: function (response) {
				window.parent.scrollTo(0, 0);
				$('#tariffaModal').modal('toggle');
				TERAPISTIgetTariffe();  
			  },
			  error: function () {
				alert("error");
			  }
			});	
	}	
}