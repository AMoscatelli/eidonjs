var rowData= [];
var id_CategoriaT_cache;

/* function FISSO(){
   if($('#percentuale:checked').val() === "on"&& $('#fisso:checked').val()==="on"){
	  $("#percentuale").prop('checked', false);
   }else if($('#percentuale:checked').val() !== "on"&& $('#fisso:checked').val()!=="on"){
	  $("#percentuale").prop('checked', true);
   }
}

function PERCENTUALE(){
   if($('#percentuale:checked').val() === "on"&& $('#fisso:checked').val()==="on"){
	  $("#fisso").prop('checked', false);
   }else if($('#percentuale:checked').val() !== "on"&& $('#fisso:checked').val()!=="on"){
	  $("#fisso").prop('checked', true);
   }
} */

function initialMenu() {
   document.getElementById("aggiungi").style.display = "none";
   document.getElementById("delete").style.display = "none";
   document.getElementById("edit").style.display = "none";
   document.getElementById("update").style.display = "none";
               
}

function UtilityCategoriaTinitGrid(Query, GridName){
   $.ajax({
			type: 'post',
			url: '../php/mysql/Query_Select.php',
			data: { 
						'query': Query
			},
			success: function (response) {
						//alert(response);
						obj = JSON.parse(response);
						showGrid(obj, GridName);
			},
			error: function () {
						alert("error");
			}
   });
}


function showGrid(obj, GridName) {
               
               $grid = $("#"+GridName+"");
               $grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "tipoCategoria", label: "Tipo"}   
        ],
		  autowidth: true,
        rowNum: 12,
		  pager: "#packagePager",
        viewrecords: true,
        rownumbers: false,
                              caption:"Cerca:",
        height: "auto",
        sortname: "Tariffe",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid);
            
			id_CategoriaT_cache=rowData.ID;
			nome=rowData.stanza;
			$("#tipoT").focus();
			$("#tipoT").val(rowData.tipoCategoria);
			$("#quota").focus();
			$("#quota").val(rowData.quota);
			(rowData.percentuale==0) ? $("#fisso").prop('checked', true) : $("#fisso").prop('checked', false) ;
			(rowData.fissoMensile==0) ? $("#fissoMensile").prop('checked', true) : $("#fissoMensile").prop('checked', false) ;
			(rowData.ritenutaAcc==0) ? $("#ritAcc").prop('checked', true) : $("#ritAcc").prop('checked', false) ;
														 
			lockFields();
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("save").style.display = "none";
			document.getElementById("edit").style.display = "block";
			document.getElementById("update").style.display = "none";
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

               
}

function lockFields(){
   document.getElementById("tipoT").readOnly = true;
   document.getElementById("quota").readOnly = true;
   $("#fisso").prop("readonly", true);
}             

function UtilityCategoriaTadd(){
               
   unlockFields();
   cancelFields();
   initialMenu();
   document.getElementById("save").style.display = "block";
}

function unlockFields(){
   document.getElementById("tipoT").readOnly = false;
   document.getElementById("quota").readOnly = false;
   $("#fisso").prop("readonly", false);
}             

function cancelFields() {
	  document.getElementById("tipoT").value="";
	  document.getElementById("quota").value="";
	  $("#tipoT").focus();
}

function UtilityCategoriaTmodalDelete() {
   $('#EliminaCategoria').modal();
}

function UtilityCategoriaTdelete(){
   
   var Query_agenda= "SELECT t.nomeCognome_t FROM Terapisti AS t JOIN CategoriaT AS s ON t.ID=s.ID WHERE s.ID = "+id_CategoriaT_cache;
   $.ajax({
		type: 'post',
		url: '../php/mysql/Query_Select.php',
		data: { 
			'query': Query_agenda
		},
		success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			if(obj.length!==0){
			   //alert("Impossibile eliminare: il paziente ha appuntamenti pianificati in agenda")
			   //$('#EliminaPaziente').modal('hide');
			   $('#ImpossibileEliminare').modal();
			}else{
			   var Query= "DELETE FROM `CategoriaT` WHERE `tipoCategoria` = '"+$("#tipoT").val()+"' AND `ID` = '"+id_CategoriaT_cache+"'";
			   $.ajax({
						type: 'post',
						url: '../php/mysql/Query_Insert_Delete.php',
						data: { 
									 'query': Query
						},
						success: function (response) {
							 UtilityCategoriaTinitGrid('Select * from CategoriaT', 'jqGrid');
							 UTILITYdeleted();
						 },
						error: function () {
							 alert("error");
						}
					  });
				}
											 //showGrid(obj, GridName);
		},
		error: function () {
					//alert("error");
		}
   });
               
}
function UTILITYdeleted(){
               
   unlockFields();
   cancelFields();
   initialMenu();
   document.getElementById("save").style.display = "block";
}

function UtilityCategoriaTedit(){
	initialMenu();
	document.getElementById("update").style.display = "block";
	document.getElementById("save").style.display = "none";
	unlockFields();
}

function UtilityCategoriaTsalva() {
	
	if (validate()) {
		
		if($('#fisso:checked').val() === "on"){
			a=0;
		}else{ 
			a=1;
		}
		if($('#ritAcc:checked').val() === "on"){
			b=0;
		}else{ 
			b=1;
		}
		if($('#fissoMensile:checked').val() === "on"){
			c=0;
		}else{ 
			c=1;
		}
		
		var Query="INSERT INTO `CategoriaT` (`ID`, `tipoCategoria`, `quota`, `percentuale`, `ritenutaAcc`, `fissoMensile`) VALUES (NULL, '"+$("#tipoT").val()+"','"+$("#quota").val()+"',"+a+","+b+","+c+")";
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			UtilityCategoriaTinitGrid('Select * from CategoriaT', 'jqGrid');
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("edit").style.display = "block";
			document.getElementById("save").style.display = "none";
			lockFields();
			objID=JSON.parse(response);
			id_CategoriaT_cache=objID[0].ID;
			$('#AssociazioneOK').modal();
		  },
		  error: function () {
			alert("error");
		  }
		});	
	}else{	
		$('#validate').modal();
	}
}

function validate(){
	var tipoT_n=document.getElementById('tipoT').value;
	var quota_n=document.getElementById('quota').value;
	if (tipoT_n==""|| quota_n=="") {
		return false;
	}else{
		return true;
	}
}

function UtilityCategoriaTmodifica() {
		
	if (validate) {
		if($('#fisso:checked').val() === "on"){
			a=0;
		}else{ 
			a=1;
		}
		if($('#ritAcc:checked').val() === "on"){
			b=0;
		}else{ 
			b=1;
		}
		if($('#fissoMensile:checked').val() === "on"){
			c=0;
		}else{ 
			c=1;
		}
		var Query= "UPDATE `CategoriaT` SET `tipoCategoria` = '"+$("#tipoT").val()+"', `quota` = '"+$("#quota").val()+"', `percentuale` = '"+a+" , `ritenutaAcc` = '"+b+" , `fissoMensile` = '"+c+"'  WHERE `ID` = '"+id_CategoriaT_cache+"'";
		
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  
			UtilityCategoriaTinitGrid('Select * from CategoriaT', 'jqGrid');
			
		  },
		  error: function () {
			alert("error");
		  }
	});
	}else{
	
		$('#validate').modal();
	}
	document.getElementById("aggiungi").style.display = "block";
	document.getElementById("delete").style.display = "block";
	document.getElementById("edit").style.display = "block";
	document.getElementById("update").style.display = "none";
	lockFields();
	
}