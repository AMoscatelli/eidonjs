var rowData= [];
var id_Tariffa_cache;
function initialMenu() {
	document.getElementById("aggiungi").style.display = "none";
	document.getElementById("delete").style.display = "none";
	document.getElementById("edit").style.display = "none";
	document.getElementById("update").style.display = "none";
	
}

function UtilityTARIFFAinitGrid(Query, GridName){
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			showGrid(obj, GridName);
		  },
		  error: function () {
			alert("error");
		  }
	});
}


function showGrid(obj, GridName) {
	
	$grid = $("#"+GridName+"");
	$grid.jqGrid('GridUnload');
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "tariffa", label: "Tariffe"}
			
            
        ],
		autowidth: true,
        rowNum: 12,
		pager: "#packagePager",
        viewrecords: true,
        rownumbers: false,
		caption:"Cerca:",
        height: "auto",
        sortname: "Tariffe",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            rowData = $(this).jqGrid("getLocalRow", rowid);
            
			id_Tariffa_cache=rowData.ID;
			nome=rowData.stanza;
			$("#tariffa").focus();
			$("#tariffa").val(rowData.tariffa);
						
			lockFields();
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("save").style.display = "none";
			document.getElementById("edit").style.display = "block";
			document.getElementById("update").style.display = "none";
        }
    });
    $grid.jqGrid("navGrid", "#packagePager",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });

	
}

function lockFields(){
	document.getElementById("tariffa").readOnly = true;
}	

function UtilityTARIFFAadd(){
	
	unlockFields();
	cancelFields();
	initialMenu();
	document.getElementById("save").style.display = "block";
}

function unlockFields(){
	document.getElementById("tariffa").readOnly = false;
}	

function cancelFields() {
		document.getElementById("tariffa").value="";
		$("#tariffa").focus();
}

function UtilityTARIFFAmodalDelete() {
	$('#EliminaTariffa').modal();
}

function UtilityTARIFFAdelete(){
	
	var Query_agenda= "SELECT t.nomeCognome_t FROM Terapisti AS t JOIN Tariffa AS s ON t.idTariffa=s.ID WHERE s.ID = "+id_Tariffa_cache;
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query_agenda
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			if(obj.length!==0){
				//alert("Impossibile eliminare: il paziente ha appuntamenti pianificati in agenda")
				//$('#EliminaPaziente').modal('hide');
				$('#ImpossibileEliminare').modal();
			}else{
				var Query= "DELETE FROM `Tariffa` WHERE `tariffa` = '"+$("#tariffa").val()+"' AND `ID` = '"+id_Tariffa_cache+"'";
				$.ajax({
					  type: 'post',
					  url: '../php/mysql/Query_Insert_Delete.php',
					  data: { 
						'query': Query
					  },
					  success: function (response) {
						UtilityTARIFFAinitGrid('Select * from Tariffa', 'jqGrid');
						UTILITYdeleted();
						},
					  error: function () {
						alert("error");
					  }
					});
						}
						//showGrid(obj, GridName);
		  },
		  error: function () {
			//alert("error");
		  }
	});
	
}
function UTILITYdeleted(){
	
	unlockFields();
	cancelFields();
	initialMenu();
	document.getElementById("save").style.display = "block";
}

function UtilityTARIFFAsalva() {
	
	if (validate()) {
		var Query="INSERT INTO `Tariffa` (`ID`, `tariffa`) VALUES (NULL, '"+$("#tariffa").val()+"')";
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			UtilityTARIFFAinitGrid('Select * from Tariffa', 'jqGrid');
			document.getElementById("aggiungi").style.display = "block";
			document.getElementById("delete").style.display = "block";
			document.getElementById("edit").style.display = "block";
			document.getElementById("save").style.display = "none";
			lockFields();
			objID=JSON.parse(response);
			id_Tariffa_cache=objID[0].ID;
			$('#AssociazioneOK').modal();
		  },
		  error: function () {
			alert("error");
		  }
		});	
	}else{	
		$('#validate').modal();
	}
}

function validate(){
	var nomeT_n=document.getElementById('tariffa').value;
	if (nomeT_n=="") {
		return false;
	}else{
		return true;
	}
}


function UtilityTARIFFAedit(){
	initialMenu();
	document.getElementById("update").style.display = "block";
	document.getElementById("save").style.display = "none";
	unlockFields();
}


function UtilityTARIFFAmodifica() {
		
	if (validate) {
		
		var Query= "UPDATE `Tariffa` SET `tariffa` = '"+$("#tariffa").val()+"' WHERE `ID` = "+rowData.ID;
		
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			  
			UtilityTARIFFAinitGrid('Select * from Tariffa', 'jqGrid');
			
		  },
		  error: function () {
			alert("error");
		  }
	});
	}else{
	
		$('#validate').modal();
	}
	document.getElementById("aggiungi").style.display = "block";
	document.getElementById("delete").style.display = "block";
	document.getElementById("edit").style.display = "block";
	document.getElementById("update").style.display = "none";
	lockFields();
	
}