﻿function STAMPAtutti(){
	
	$("#annullati").prop('checked', false);
	$("#confermati").prop('checked', false);
	$("#nonGestiti").prop('checked', false);
	
}

function STAMPAannullati(){
	$("#tutti").prop('checked', false);
	
	if($('#annullati:checked').val() === "on" && $('#confermati:checked').val()==="on" && $('#nonGestiti:checked').val() === "on"){
		STAMPAtutti()
		$("#tutti").prop('checked', true);
	}

	
}

function STAMPAconfermati(){
	$("#tutti").prop('checked', false);
	if($('#annullati:checked').val() === "on" && $('#confermati:checked').val()==="on" && $('#nonGestiti:checked').val() === "on"){
		STAMPAtutti()
		$("#tutti").prop('checked', true);
	}
	
}

function STAMPAnonGestiti(){
	$("#tutti").prop('checked', false);
	if($('#annullati:checked').val() === "on" && $('#confermati:checked').val()==="on" && $('#nonGestiti:checked').val() === "on"){
		STAMPAtutti()
		$("#tutti").prop('checked', true);
	}
	
	
}

var obj_global;

$("#start").focusout(function(){
    alert('lostfocus');
});

function STAMPAgetTerapisti(){
	$("#terapistaSelect").empty();
	var Query = "SELECT t.*, c.fissoMensile FROM terapisti as t join categoriat as c ON t.idCategoria = c.ID order  by nomeCognome_t DESC";
	var stanza = document.getElementById("terapistaSelect");
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			var obj = JSON.parse(response);
			obj_global = obj;
			if(obj.length>0){
				var option = document.createElement("option");
				option.value = 9999;
				option.text = "Tutti";
				stanza.add(option);
			for(var i=obj.length-1; i>=0;i--){
				if(obj[i].nomeCognome_t!=='Administrator'){
					var option = document.createElement("option");
					option.value = obj[i].ID;
					option.text = obj[i].nomeCognome_t;
					stanza.add(option);
				}
			}
			}
			
			
		  },
		  error: function () {
			alert("Non sono riuscito a trovare Terapisti");
		  }
	});
}

function STAMPAgetTerapista(ID){

$("#terapistaSelect").empty();
var Query = "SELECT * FROM terapisti where ID = "+ID;
var stanza = document.getElementById("terapistaSelect");
$.ajax({
	  type: 'post',
	  url: '../php/mysql/Query_Select.php',
	  data: { 
		'query': Query
	  },
	  success: function (response) {
		var obj = JSON.parse(response);
		if(obj.length>0){
			
		for(var i=obj.length-1; i>=0;i--){
			var option = document.createElement("option");
			option.value = obj[i].ID;
			option.text = obj[i].nomeCognome_t;
			stanza.add(option);
		}
		}
		
		
	  },
	  error: function () {
		alert("Non sono riuscito a trovare Terapisti");
	  }
});
}
