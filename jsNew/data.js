

function convertiData(input,frequenza,index){
					 // Unixtimestamp
			var frequenza;
			
			var unixtimestamp = input;
			
			if (index==1){
				
				var date_par = input.substring(input.indexOf(" ")+1,input.length);
				var giorno_par = date_par.substring(0,2);
				var mese_par = date_par.substring(3,5);
				var anno_par = date_par.substring(6,10);
				var unixtimestamp = anno_par+"-"+mese_par+"-"+giorno_par;
				
			}
			// Months array
			var months_arr = ['01','02','03','04','05','06','07','08','09','10','11','12'];
			// Convert timestamp to milliseconds
			var date = new Date(unixtimestamp);
			
			var offset = date.getTimezoneOffset() / 60;
			
			// Year
			var year = date.getFullYear();
			// Month
			var month = months_arr[date.getMonth()];
			// Day
			var day = date.getDate();
			
			if (day<10) {
				
				day = "0"+date.getDate();
			}
			// Hours
			
			if (index==4) {
			var hours = date.getHours()+offset;
			
			if(hours<10){
				hours= "0"+(date.getHours()+offset);
			}
			} else {
				var hours = date.getHours();
			
			if(hours<10){
				hours= "0"+(date.getHours());
			}
				
			}
			// Minutes
			var minutes = "0" + date.getMinutes();
			// Seconds
			var seconds = "0" + date.getSeconds();
			// Display date time in MM-dd-yyyy h:m:s format
			var convdataTime = year+'-'+month+'-'+day+' '+hours + ':' + minutes.substr(-2);
			 
			var convdataTime2 = day+'-'+month+'-'+year;
			
			var convdataTime4 = year+'-'+month+'-'+day;
			 
			var convdataTime3 = hours + ':' + minutes.substr(-2);
			
			if (frequenza=="full") {
				//restituisce il valore completo di anno mese giorno e ora
				return convdataTime
				
			} else if (frequenza=="anno"){
				//restituisce il valore completo solo di anno mese giorno
				return convdataTime2
				
			} else if (frequenza=="anno2"){
				//restituisce il valore completo solo di anno mese giorno
				return convdataTime4
				
			} else if (frequenza=="ora"){
				//restituisce il valore completo solo dell'ora
				return convdataTime3
			}
			
}

function calcolaMs(data,index){
	
	if (index==1){
	
	var data2 = data.substring(data.indexOf(" ")+1, data.lastIndexOf(" "));
	var orafull = data.substring(data.lastIndexOf(" ")+1, data.length);
	} else if (index==2) {
		
	var data2 = data.substring(data.indexOf(" ")+1, data.length);
	var orafull = "13:00";
	}
	
	var giorno = data2.substring(0,2);
	var mese = data2.substring(3,5);
	var anno = data2.substring(6,10);
	
	var data3 = parseInt(Date.parse(new Date(anno+"-"+mese+"-"+giorno+"T"+orafull+":00")));
	
	return data3
}


function calcolaDataDurata(data,durata,index){
	
	var data2 = data.substring(data.indexOf(" ")+1, data.lastIndexOf(" "));
	
	var giorno = data2.substring(0,2);
	var mese = data2.substring(3,5);
	var anno = data2.substring(6,10);
	var orafull = data.substring(data.lastIndexOf(" ")+1, data.length);
	var data3 = parseInt(Date.parse(new Date(anno+"-"+mese+"-"+giorno+"T"+orafull+":00")));
	
	var segnodurata = durata.substring(0,1);
	
	if(segnodurata=="-"){
	
	var durata2 = (parseInt(durata.substring(1,durata.length))*60*1000);//+3600000;
	
	} else {
	
	var durata2 = (parseInt(durata)*60*1000);//+3600000;
	}
	
	
	if(segnodurata=="-"){
	
	var somma_data = (data3-durata2);
	
	} else {
	
	var somma_data = (data3+durata2);
	
	}
	
	if (index==1){
		
		var full = convertiData(somma_data,"anno");
		
	} else if (index=2){
		
		var full = convertiData(somma_data,"anno2");
	}
		
	
	var full2 = convertiData(somma_data,"ora",2);
	
	full3 = full + " " + full2 + ":00"
	return full3
	
}


function calcolaRicorrenza(dataInizio,dataFine,ricorrenza){
	
	var data2 = dataInizio.substring(0,dataInizio.indexOf(" "));
	var giorno = data2.substring(0,2);
	var mese = data2.substring(3,5);
	var anno = data2.substring(6,10);
	var orafull = dataInizio.substring(dataInizio.lastIndexOf(" ")+1, dataInizio.length);
	var orafull2 = parseInt(orafull.substring(0,orafull.indexOf(":")));
	//if (orafull2<10){
		//orafull2 = "0"+orafull.toString();
	//} else {
		//orafull2 = orafull;
	//}
	//var data2ms = parseInt(Date.parse(new Date(anno+"-"+mese+"-"+giorno+"T"+orafull2)));
	
	var data2ms = parseInt(Date.parse(new Date(anno+"-"+mese+"-"+giorno+"T"+orafull)));
	
	
	var data3 = dataFine.substring(dataFine.indexOf(" ")+1,dataFine.length);
	var giorno2 = data3.substring(0,2);
	var mese2 = data3.substring(3,5);
	var anno2= data3.substring(6,10);
	//var data3ms = parseInt(Date.parse(new Date(anno2+"-"+mese2+"-"+giorno2+"T"+orafull2)));
	var data3ms = parseInt(Date.parse(new Date(anno2+"-"+mese2+"-"+giorno2+"T"+orafull)));
	
	var tot = data3ms - data2ms;
	
	if (ricorrenza=="Giornaliera"){
		var finale = Math.floor(tot/86400000);
	} else if (ricorrenza=="Settimanale"){
		var finale = Math.floor(tot/604800000);
	} else if (ricorrenza=="Mensile"){
	var finale = Math.floor(tot/2592000000);
	}
	return finale
}

function calcolaDateRicorrenza(dataInizio,numRicorrenza,ricorrenza){
	
	if (ricorrenza=="Giornaliera"){
		ricorrenza="day";
	} else if (ricorrenza=="Settimanale"){
		ricorrenza="week";
	} else if (ricorrenza=="Mensile") {
		ricorrenza="month";
	}
	
	var data2 = dataInizio.substring(0,dataInizio.indexOf(" "));
	var giorno = data2.substring(0,2);
	var mese = data2.substring(3,5);
	var anno = data2.substring(6,10);
	var orafull = dataInizio.substring(dataInizio.lastIndexOf(" ")+1, dataInizio.length);
	var orafull2 = parseInt(orafull.substring(0,orafull.indexOf(":")));
//	if (orafull2<10){
//		orafull2 = "0"+orafull.toString();
//	} else {
//		orafull2 = orafull;
//	}
	//var data2ms = new Date(anno+"-"+mese+"-"+giorno+"T"+orafull2);
	var data2ms = new Date(anno+"-"+mese+"-"+giorno+"T"+orafull);
	var data_finale = moment(data2ms).add(numRicorrenza,ricorrenza).format("dddd DD-MM-YYYY HH:mm");
	


	return data_finale
}

//converte una data in formatto gg/mm/yyyy in yyyy/mm/gg
function getMysqlDate(dataValue, separatore){
	if(separatore === "."){
		var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
		return dataValue.replace(pattern,'$3-$2-$1');
	}
	if(separatore === "/"){
		var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
		return dataValue.replace(pattern,'$3-$2-$1');
	}
	if(separatore === "-"){
		var pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
		return dataValue.replace(pattern,'$3-$2-$1');
	}
	if(separatore === ":"){
		var pattern = /(\d{2})\:(\d{2})\:(\d{4})/;
		return dataValue.replace(pattern,'$3-$2-$1');
	}
}