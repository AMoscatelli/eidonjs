﻿﻿

var intestazione = 140;
var leftAlign = 20;
var inizioRow = 50;
var nome;
var paziente;
var row=0;
var nextRow = 5;
var spazio = 10;
var totale = 0
var totaleSt = 0;
var totaleTe = 0;
var subtotale = 0;
var subtotaleSt = 0;
var subtotaleTe = 0;
var addTotali = false;
var preview = false;
var pagati = false;
var noDetails = false;
var written = true;

function STAMPAanteprime(){
	preview = $('#preview:checked').val() === "on" ? true : false;
}
function STAMPAregPagamenti(){
	pagati = $('#pagati:checked').val() === "on" ? true : false;
}
function STAMPAsoloTotali(){
	noDetails = $('#detail:checked').val() === "on" ? true : false;
}

function formatComma(amount) {
	var delimiter = "."; 
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3) {
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + ',' + d; }
	amount = minus + amount;
	return amount;
}

function PDFfatturaDoposcola(user){

	var mese = $('#startDoposcuola').val();
	
	var inizio = $('#start').val();
	var fine = $('#end').val();
	if(inizio === "" || fine === ""){
		$('#attenzioneModal').modal();
	}else{
	$.LoadingOverlay("show");
	var annullati = $('#annullati:checked').val() === "on" ? true : false;
	var confermati = $('#confermati:checked').val() === "on" ? true : false;
	var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
	var tutti = $('#tutti:checked').val() === "on" ? true : false;	

	totale = 0
	totaleSt = 0;
	totaleTe = 0;
	addTotali = false;

	var doc = new jsPDF('landscape');

	if(user === undefined){
		if(tutti){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato < 3 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if(annullati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";	
		}else if (confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti && confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti && annullati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0  ORDER by t.nomeCognome_t";
		}else if (annullati && confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}
	}else{
		if(tutti){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato < 3 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if(annullati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";	
		}else if (confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti && confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (nonGestiti && annullati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}else if (annullati && confermati){
			var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join join joinPazienteDopoScuola AS jpd ON jpd.idDopoScuola = a.idDopoScuola join paziente AS p ON p.idPaziente = jpd.IdPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND t.ID = "+user+" AND NOT a.idDoposcuola <> 0 ORDER by t.nomeCognome_t";
		}

	}

	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			corpoDoposcuola(obj, doc);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
}
}

function PDFfattura(user){
	
var inizio = $('#start').val();
var fine = $('#end').val();
if(inizio === "" || fine === ""){
	$('#attenzioneModal').modal();
}else{
$.LoadingOverlay("show");
var annullati = $('#annullati:checked').val() === "on" ? true : false;
var confermati = $('#confermati:checked').val() === "on" ? true : false;
var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
var tutti = $('#tutti:checked').val() === "on" ? true : false;	

totale = 0
totaleSt = 0;
totaleTe = 0;
addTotali = false;

	

	

doc = new jsPDF('landscape');

if(user === '9999'){
	if(tutti){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato < 3 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if(annullati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";	
	}else if (confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti && confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti && annullati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (annullati && confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+"' AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC and p.idpaziente ASC and a.start DESC";
	}
}else{
	if(tutti){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato < 3 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if(annullati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";	
	}else if (confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti && confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59'  AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (nonGestiti && annullati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}else if (annullati && confermati){
		var Query = "Select a.id, t.nomeCognome_t, t.ritAcconto, a.start, a.end, p.nomeCognome, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59'  AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by t.nomeCognome_t ASC , p.idpaziente ASC , a.start DESC";
	}

}
$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			corpo(obj, doc);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
	
	
}	

	
}

function PDFfatturaPaziente(user){

var inizio = $('#start').val();
var fine = $('#end').val();
if(inizio === "" || fine === ""){
	$('#attenzioneModal').modal();
}else{
$.LoadingOverlay("show");
var annullati = $('#annullati:checked').val() === "on" ? true : false;
var confermati = $('#confermati:checked').val() === "on" ? true : false;
var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
var tutti = $('#tutti:checked').val() === "on" ? true : false;	

totale = 0
totaleSt = 0;
totaleTe = 0;
addTotali = false;

var doc = null;
doc = new jsPDF('landscape');

if(user === '9999'){
	if(tutti){       
		var Query = "Select a.id, p.nomeCognome, a.start, a.end, t.nomeCognome_t, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if(annullati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";
	}else if (confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";
	}else if (nonGestiti){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if (nonGestiti && confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if (nonGestiti && annullati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";
	}else if (annullati && confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";
	}
}else{
	if(tutti){       
		var Query = "Select a.id, p.nomeCognome, a.start, a.end, t.nomeCognome_t, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if(annullati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";	
	}else if (confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if (nonGestiti){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if (nonGestiti && confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato <= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC , t.nomeCognome_t ASC,  a.start DESC";
	}else if (nonGestiti && annullati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato = 0 AND a,stato = 2 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59' AND t.ID = "+user+" AND a.idDoposcuola = 0 OORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}else if (annullati && confermati){
		var Query = "Select a.id, t.nomeCognome_t, a.start, a.end, p.nomeCognome, t.ritAcconto, a.stato, tari.tariffa, cat.quota, cat.percentuale, cat.fissoMensile from agenda AS a join Terapisti AS t ON a.idTerapista = t.ID join Tariffa AS ta ON a.idTariffa = ta.ID join paziente AS p ON p.idPaziente = a.idPaziente join Tariffa AS tari ON tari.ID = a.idTariffa join CategoriaT AS cat ON cat.ID = t.idCategoria WHERE not eventoCancellato = 1 AND a.stato >= 1 AND a.start >= '"+inizio+"' AND a.end <= '"+fine+" 23:59'  AND t.ID = "+user+" AND a.idDoposcuola = 0 ORDER by p.idpaziente ASC ,  t.nomeCognome_t ASC, a.start DESC";
	}

}





$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			corpoPaziente(obj, doc);
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});
}
	
}

function corpoDoposcuola(obj, doc){
	
	//Intestazione Report:
	var inizio = $('#start').val();
	var fine = $('#end').val();
	var annullati = $('#annullati:checked').val() === "on" ? true : false;
	var confermati = $('#confermati:checked').val() === "on" ? true : false;
	var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
	var tutti = $('#tutti:checked').val() === "on" ? true : false;	
	
		doc.setFont("times");
		doc.setFontSize(40);
		doc.text(70, 100, "IGEA 1.0 - Report Doposcuola")
		doc.setLineWidth(1);
		doc.line(leftAlign+20, 110, 250, 110);
		doc.setFontSize(20);
		if (nonGestiti && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e non Gestiti dal "+inizio+" al "+fine);
		}else if (nonGestiti && annullati){
			doc.text(60, 120, "Appuntamenti Annullati e non Gestiti dal "+inizio+" al "+fine);
		}else if (annullati && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e Annullati dal "+inizio+" al "+fine);
		}else if(tutti){
			doc.text(70, 120, "Tutti gli appuntamenti dal "+inizio+" al "+fine);
		}else if(annullati){
			doc.text(70, 120, "Appuntamenti Annullati dal "+inizio+" al "+fine);
		}else if (confermati){
			doc.text(70, 120, "Appuntamenti Confermati dal "+inizio+" al "+fine);
		}else if (nonGestiti){
			doc.text(70, 120, "Appuntamenti non Gestiti dal "+inizio+" al "+fine);
		} 
		
		
	for(var i =0 ; i < obj.length; i++)	{
		if(nome === undefined || nome !== obj[i].nomeCognome_t){
			if(addTotali){
				doc.setLineWidth(1);
				doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
				doc.setFont("times");
				doc.setFontSize(12);
				doc.setFontType("italic");
				doc.text(leftAlign, inizioRow+150, "");
				doc.text(leftAlign+45, inizioRow+150, "");
				doc.text(leftAlign+90, inizioRow+150, "");
				doc.text(leftAlign+120, inizioRow+150, "");
				doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
				doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
				doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
				totale = 0
				totaleSt = 0;
				totaleTe = 0;
				addTotali = false;
			}
			doc.addPage();
			doc.setFont("times");
			doc.setFontSize(20);
			doc.setFontType("italic");
			if(obj[i].percentuale==="1"){
				doc.text(leftAlign, inizioRow-15, "Terapista: "+obj[i].nomeCognome_t+" - Calcolo applicato: "+obj[i].quota+" % sul totale.");
			}else{
				doc.text(leftAlign, inizioRow-15, "Terapista: "+obj[i].nomeCognome_t+" - Calcolo applicato: Quota Fissa di € "+parseFloat(obj[i].quota).toFixed(2)+" sul totale.");
			}
			nome=obj[i].nomeCognome_t;
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Paziente");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.");
			row=0;
			addTotali=true;
			
		}
		//inizioRow += (5);
		if(row===150){
			doc.addPage();
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Paziente");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.");
			row=0;
		}
		doc.setFont("times");
		doc.setFontSize(10);
		if(obj[i].stato==2){
			doc.setFontType("bolditalic");
			doc.setTextColor(255,0,0);
		}else{
			doc.setFontType("bold");
			doc.setTextColor(0,0,0);
		}
		var dateSt = new Date(obj[i].start);
		var dateEn = new Date(obj[i].end);
		var ora = dateEn-dateSt;
		var ora = ora/3600000;
		//1 = calcolo in percentuale, 0 quota Fissa
		if(obj[i].percentuale==="1"){
			var quota_user = parseFloat(obj[i].quota/100);
			var quota_studio = parseFloat(1 - quota_user);
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa)*ora)*quota_studio;
			totaleTe += (parseFloat(obj[i].tariffa)*ora)*quota_user;
		}else{
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa - obj[i].quota)*ora);
			totaleTe += (parseFloat(obj[i].quota)*ora);	
		}
		doc.text(leftAlign, inizioRow+row, obj[i].nomeCognome);
		doc.text(leftAlign+45, inizioRow+row, convertiData(obj[i].start,"anno",0)+" - "+convertiData(obj[i].start,"ora",0));
		doc.text(leftAlign+90, inizioRow+row, String(ora.toFixed(2)));
		doc.text(leftAlign+120, inizioRow+row, obj[i].tariffa);
		doc.text(leftAlign+150, inizioRow+row, formatComma((parseFloat(obj[i].tariffa)*ora).toFixed(2)));
		if(obj[i].percentuale==="1"){
			var quota_user = parseFloat(obj[i].quota/100);
			var quota_studio = parseFloat(1 - quota_user);
			doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_studio).toFixed(2)));
			doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_user).toFixed(2)));
		}else{
			doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa - obj[i].quota)*ora)).toFixed(2)));
			doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].quota)*ora)).toFixed(2)));
		}
		row+=5;
	
		
	}
	
		doc.setLineWidth(1);
		doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
		doc.setFont("times");
		doc.setFontSize(12);
		doc.setFontType("italic");
		doc.text(leftAlign, inizioRow+150, "");
		doc.text(leftAlign+45, inizioRow+150, "");
		doc.text(leftAlign+90, inizioRow+150, "");
		doc.text(leftAlign+120, inizioRow+150, "");
		doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
		doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
		doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
	// Output as Data URI
	$.LoadingOverlay("hide");
	if(preview){
		doc.output('datauri');
	}else{
		doc.save('Report_Doposcuola_'+inizio+'_'+fine+'.pdf')
	}
	if(pagati && !preview){
		segnaAgendaPagati(obj);
	}
}


function corpo(obj, doc){
	
	//Intestazione Report:
	var inizio = $('#start').val();
	var fine = $('#end').val();
	var annullati = $('#annullati:checked').val() === "on" ? true : false;
	var confermati = $('#confermati:checked').val() === "on" ? true : false;
	var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
	var tutti = $('#tutti:checked').val() === "on" ? true : false;	
	var ritenuta;
	var fisso = 0;
	paziente = undefined;
	subtotale = 0;
	subtotaleSt = 0;
	subtotaleTe = 0;
	row = 0;
	nome = undefined;
	
		doc.setFont("times");
		doc.setFontSize(40);
		doc.text(70, 100, "IGEA 1.0 - Report Terapisti")
		doc.setLineWidth(1);
		doc.line(leftAlign+20, 110, 250, 110);
		doc.setFontSize(20);
		if (nonGestiti && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e non Gestiti dal "+inizio+" al "+fine);
		}else if (nonGestiti && annullati){
			doc.text(60, 120, "Appuntamenti Annullati e non Gestiti dal "+inizio+" al "+fine);
		}else if (annullati && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e Annullati dal "+inizio+" al "+fine);
		}else if(tutti){
			doc.text(70, 120, "Tutti gli appuntamenti dal "+inizio+" al "+fine);
		}else if(annullati){
			doc.text(70, 120, "Appuntamenti Annullati dal "+inizio+" al "+fine);
		}else if (confermati){
			doc.text(70, 120, "Appuntamenti Confermati dal "+inizio+" al "+fine);
		}else if (nonGestiti){
			doc.text(70, 120, "Appuntamenti non Gestiti dal "+inizio+" al "+fine);
		} 
		
		
	for(var i =0 ; i < obj.length; i++)	{
		if(row===130){
			doc.addPage();
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Paziente");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.");
			doc.text(leftAlign+220, inizioRow-5, "Rit. Acc.");
			doc.text(leftAlign+240, inizioRow-5, "DA pagare");
			row=0;
		}
		if(paziente !== undefined && paziente !== obj[i].nomeCognome){
			//aggiungo il valore di sub totale per Paziente
			doc.setLineWidth(0.5);
			doc.line(leftAlign, inizioRow+row-4, 280, inizioRow+row-4);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.setTextColor(0,0,0);
			doc.text(leftAlign, inizioRow+row, "");
			doc.text(leftAlign+45, inizioRow+row, "");
			doc.text(leftAlign+90, inizioRow+row, "");
			doc.text(leftAlign+120, inizioRow+row, "");
			var aa1 = formatComma(subtotale.toFixed(2));
			var aa2 = formatComma(subtotaleSt.toFixed(2));
			var aa3 = formatComma(subtotaleTe.toFixed(2));
			doc.text(leftAlign+150, inizioRow+row, aa1);
			doc.text(leftAlign+170, inizioRow+row, aa2);
			doc.text(leftAlign+200, inizioRow+row, aa3);
			subtotale = 0;
			subtotaleSt = 0;
			subtotaleTe = 0;
			written=true;
			row +=5;
			paziente = obj[i].nomeCognome;
			ritenuta = obj[i].ritAcconto;
			fisso = obj[i].fissoMensile;
			console.log(i+' - '+obj[i].nomeCognome+' - '+obj[i].tariffa);
			var subdateSt = new Date(obj[i].start);
			var subdateEn = new Date(obj[i].end);
			var subora = subdateEn-subdateSt;
			var subora = subora/3600000;
			if(obj[i].fissoMensile==="0"){
				//var quota_user = parseFloat(obj[i].quota/100);
				//var quota_studio = parseFloat(1 - quota_user);
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa)*subora);//*quota_studio;
				subtotaleTe += 0;//(parseFloat(obj[i].tariffa)*ora)*quota_user;
			}else if(obj[i].percentuale==="1"){
				var quota_user = parseFloat(obj[i].quota/100);
				var quota_studio = parseFloat(1 - quota_user);
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa)*subora)*quota_studio;
				subtotaleTe += (parseFloat(obj[i].tariffa)*subora)*quota_user;
			}else{
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa - obj[i].quota)*subora);
				subtotaleTe += (parseFloat(obj[i].quota)*subora);	
			}
		}else {
			console.log(i+' - '+obj[i].nomeCognome+' - '+obj[i].tariffa);
			var subdateSt = new Date(obj[i].start);
			var subdateEn = new Date(obj[i].end);
			var subora = subdateEn-subdateSt;
			var subora = subora/3600000;
			if(obj[i].fissoMensile==="0"){
				//var quota_user = parseFloat(obj[i].quota/100);
				//var quota_studio = parseFloat(1 - quota_user);
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa)*subora);//*quota_studio;
				subtotaleTe += 0;//(parseFloat(obj[i].tariffa)*ora)*quota_user;
			}else if(obj[i].percentuale==="1"){
				var quota_user = parseFloat(obj[i].quota/100);
				var quota_studio = parseFloat(1 - quota_user);
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa)*subora)*quota_studio;
				subtotaleTe += (parseFloat(obj[i].tariffa)*subora)*quota_user;
			}else{
				subtotale += parseFloat(obj[i].tariffa)*subora;
				subtotaleSt += (parseFloat(obj[i].tariffa - obj[i].quota)*subora);
				subtotaleTe += (parseFloat(obj[i].quota)*subora);	
			}
			
		}
		if(i==obj.length-1){
			//aggiungo il valore di sub totale per l'ultimo Paziente
			doc.setLineWidth(0.5);
			doc.line(leftAlign, inizioRow+row+1, 280, inizioRow+row+1);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.setTextColor(0,0,0);
			doc.text(leftAlign, inizioRow+row+5, "");
			doc.text(leftAlign+45, inizioRow+row+5, "");
			doc.text(leftAlign+90, inizioRow+row+5, "");
			doc.text(leftAlign+120, inizioRow+row+5, "");
			var aa1 = formatComma(subtotale.toFixed(2));
			var aa2 = formatComma(subtotaleSt.toFixed(2));
			var aa3 = formatComma(subtotaleTe.toFixed(2));
			doc.text(leftAlign+150, inizioRow+row+5, aa1);
			doc.text(leftAlign+170, inizioRow+row+5, aa2);
			doc.text(leftAlign+200, inizioRow+row+5, aa3);
			subtotale = 0;
			subtotaleSt = 0;
			subtotaleTe = 0;
		}
		if(nome === undefined || nome !== obj[i].nomeCognome_t){
			ritenuta = obj[i].ritAcconto;
			fisso = obj[i].fissoMensile;
			if(addTotali){
				doc.setLineWidth(1);
				doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
				doc.setFont("times");
				doc.setFontSize(12);
				doc.setFontType("italic");
				doc.text(leftAlign, inizioRow+150, "");
				doc.text(leftAlign+45, inizioRow+150, "");
				doc.text(leftAlign+90, inizioRow+150, "");
				doc.text(leftAlign+120, inizioRow+150, "");
				if(obj[i-1].fissoMensile=="0"){
					doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
					doc.text(leftAlign+170, inizioRow+150, '250,00');
					doc.text(leftAlign+200, inizioRow+150, formatComma((totale-250).toFixed(2)));
				}
				else{
					doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
					doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
					doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
				}
				if(obj[i-1].ritAcconto=="1"){
					var ritAcc = ((totaleTe*20)/100).toFixed(2);
					console.log(ritAcc);
					doc.text(leftAlign+220, inizioRow+150, formatComma(ritAcc));
					doc.text(leftAlign+240, inizioRow+150, formatComma((totaleTe-ritAcc).toFixed(2)));
				}
				totale = 0
				totaleSt = 0;
				totaleTe = 0;
				addTotali = false;
				
				}
				doc.addPage();
				doc.setFont("times");
				doc.setFontSize(20);
				doc.setFontType("italic");
				if(obj[i].percentuale==="1"){
					if(obj[i].fissoMensile==="1"){
						doc.text(leftAlign, inizioRow-15, "Terapista: "+obj[i].nomeCognome_t+" - Calcolo applicato: "+obj[i].quota+" % sul totale.");
					}else{
						doc.text(leftAlign, inizioRow-15, "Terapista: "+obj[i].nomeCognome_t+" - Calcolo applicato: "+obj[i].quota+" / Mese.");
					}
				}else{
					doc.text(leftAlign, inizioRow-15, "Terapista: "+obj[i].nomeCognome_t+" - Calcolo applicato: Quota Fissa di € "+parseFloat(obj[i].quota).toFixed(2)+" sul totale.");
				}
				nome=obj[i].nomeCognome_t;
				paziente = obj[i].nomeCognome;
				doc.setLineWidth(1);
				doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
				doc.setFont("times");
				doc.setFontSize(12);
				doc.setFontType("italic");
				doc.text(leftAlign, inizioRow-5, "Nome Paziente");
				doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
				doc.text(leftAlign+90, inizioRow-5, "Durata");
				doc.text(leftAlign+120, inizioRow-5, "Tariffa");
				doc.text(leftAlign+150, inizioRow-5, "Totale");
				doc.text(leftAlign+170, inizioRow-5, "Quota St.");
				doc.text(leftAlign+200, inizioRow-5, "Quota Ter.");
				doc.text(leftAlign+220, inizioRow-5, "Rit. Acc.");
				doc.text(leftAlign+240, inizioRow-5, "DA pagare");
				row=0;
				addTotali=true;
			
			}
		//inizioRow += (5);
		if(row===130){
			doc.addPage();
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Paziente");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.");
			doc.text(leftAlign+220, inizioRow-5, "Rit. Acc.");
			doc.text(leftAlign+240, inizioRow-5, "DA pagare");
			row=0;
		}
		doc.setFont("times");
		doc.setFontSize(10);
		if(obj[i].stato==2){
			doc.setFontType("bolditalic");
			doc.setTextColor(255,0,0);
		}else{
			doc.setFontType("bold");
			doc.setTextColor(0,0,0);
		}
		var dateSt = new Date(obj[i].start);
		var dateEn = new Date(obj[i].end);
		var ora = dateEn-dateSt;
		var ora = ora/3600000;
		//1 = calcolo in percentuale, 0 quota Fissa
		if(obj[i].fissoMensile==="0"){
			//var quota_user = parseFloat(obj[i].quota/100);
			//var quota_studio = parseFloat(1 - quota_user);
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa)*ora);//*quota_studio;
			totaleTe += 0;//(parseFloat(obj[i].tariffa)*ora)*quota_user;
		}else if(obj[i].percentuale==="1"){
			var quota_user = parseFloat(obj[i].quota/100);
			var quota_studio = parseFloat(1 - quota_user);
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa)*ora)*quota_studio;
			totaleTe += (parseFloat(obj[i].tariffa)*ora)*quota_user;
		}else{
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa - obj[i].quota)*ora);
			totaleTe += (parseFloat(obj[i].quota)*ora);	
		}
		if(!noDetails){
			doc.text(leftAlign, inizioRow+row, obj[i].nomeCognome);
			doc.text(leftAlign+45, inizioRow+row, convertiData(obj[i].start,"anno",0)+" - "+convertiData(obj[i].start,"ora",0));
			doc.text(leftAlign+90, inizioRow+row, String(ora.toFixed(2)));
			doc.text(leftAlign+120, inizioRow+row, obj[i].tariffa);
			doc.text(leftAlign+150, inizioRow+row, formatComma((parseFloat(obj[i].tariffa)*ora).toFixed(2)));
			if(obj[i].fissoMensile==="0"){
				//var quota_user = parseFloat(obj[i].quota/100);
				//var quota_studio = parseFloat(1 - quota_user);
				doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)).toFixed(2)));
				//doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)).toFixed(2)));
			}else if(obj[i].percentuale==="1"){
				var quota_user = parseFloat(obj[i].quota/100);
				var quota_studio = parseFloat(1 - quota_user);
				doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_studio).toFixed(2)));
				doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_user).toFixed(2)));
			}else{
				doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa - obj[i].quota)*ora)).toFixed(2)));
				doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].quota)*ora)).toFixed(2)));
			}
			row+=5;
		}else{
			if(written){
				doc.text(leftAlign, inizioRow+row, obj[i].nomeCognome);
				written= false;
			}
			
		}
		
	}
	
		doc.setLineWidth(1);
		doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
		doc.setFont("times");
		doc.setFontSize(12);
		doc.setFontType("italic");
		doc.text(leftAlign, inizioRow+150, "");
		doc.text(leftAlign+45, inizioRow+150, "");
		doc.text(leftAlign+90, inizioRow+150, "");
		doc.text(leftAlign+120, inizioRow+150, "");
		if(fisso!=undefined){
			if(fisso=="0"){
				doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
				doc.text(leftAlign+170, inizioRow+150, '250,00');
				doc.text(leftAlign+200, inizioRow+150, formatComma((totale-250).toFixed(2)));
			}else{
				doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
				doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
				doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
			}
		}else{
			doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
			doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
			doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
		}
		if(ritenuta=="1"){
			var ritAcc = ((totaleTe*20)/100).toFixed(2);
			//console.log(ritAcc);
			doc.text(leftAlign+220, inizioRow+150, formatComma(ritAcc));
			doc.text(leftAlign+240, inizioRow+150, formatComma((totaleTe-ritAcc).toFixed(2)));
		}
		
	// Output as Data URI
	$.LoadingOverlay("hide");
	if(preview){
		doc.output('datauri');
	}else{
		doc.save('Report_Terapisti.pdf')
	}
	if(pagati && !preview){
		segnaAgendaPagati(obj);
	}
}

function corpoPaziente(obj, doc){
	
	//Intestazione Report:
	var inizio = $('#start').val();
	var fine = $('#end').val();
	var annullati = $('#annullati:checked').val() === "on" ? true : false;
	var confermati = $('#confermati:checked').val() === "on" ? true : false;
	var nonGestiti = $('#nonGestiti:checked').val() === "on" ? true : false;
	var tutti = $('#tutti:checked').val() === "on" ? true : false;	
	
		doc.setFont("times");
		doc.setFontSize(40);
		doc.text(70, 100, "IGEA 1.0 - Report Pazienti")
		doc.setLineWidth(1);
		doc.line(leftAlign+20, 110, 250, 110);
		doc.setFontSize(20);
		if (nonGestiti && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e non Gestiti dal "+inizio+" al "+fine);
		}else if (nonGestiti && annullati){
			doc.text(60, 120, "Appuntamenti Annullati e non Gestiti dal "+inizio+" al "+fine);
		}else if (annullati && confermati){
			doc.text(60, 120, "Appuntamenti Confermati e Annullati dal "+inizio+" al "+fine);
		}else if(tutti){
			doc.text(80, 120, "Tutti gli appuntamenti dal "+inizio+" al "+fine);
		}else if(annullati){
			doc.text(80, 120, "Appuntamenti Annullati dal "+inizio+" al "+fine);
		}else if (confermati){
			doc.text(80, 120, "Appuntamenti Confermati dal "+inizio+" al "+fine);
		}else if (nonGestiti){
			doc.text(80, 120, "Appuntamenti non Gestiti dal "+inizio+" al "+fine);
		}else if (nonGestiti && confermati){
			doc.text(50, 120, "Appuntamenti Confermati e non Gestiti dal "+inizio+" al "+fine);
		}else if (nonGestiti && annullati){
			doc.text(50, 120, "Appuntamenti e non Gestiti dal "+inizio+" al "+fine);
		}else if (annullati && confermati){
			doc.text(50, 120, "Appuntamenti Confermati e Annullati dal "+inizio+" al "+fine);
		}
		
	for(var i =0 ; i < obj.length; i++)	{
		if(nome === undefined || nome !== obj[i].nomeCognome){
			if(addTotali){
				doc.setLineWidth(1);
				doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
				doc.setFont("times");
				doc.setFontSize(12);
				doc.setFontType("italic");
				doc.text(leftAlign, inizioRow+150, "");
				doc.text(leftAlign+45, inizioRow+150, "");
				doc.text(leftAlign+90, inizioRow+150, "");
				doc.text(leftAlign+120, inizioRow+150, "");
				doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
				doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
				doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
				totale = 0
				totaleSt = 0;
				totaleTe = 0;
				addTotali = false;
			}
			doc.addPage();
			doc.setFont("times");
			doc.setFontSize(20);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-15, "Paziente: "+obj[i].nomeCognome);
			nome=obj[i].nomeCognome;
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Terapista");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.30%");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.70%");
			row=0;
			addTotali=true;
			
		}
		//inizioRow += (5);
		if(row===150){
			doc.addPage();
			doc.setLineWidth(1);
			doc.line(leftAlign, inizioRow-10, 280, inizioRow-10);
			doc.setFont("times");
			doc.setFontSize(12);
			doc.setFontType("italic");
			doc.text(leftAlign, inizioRow-5, "Nome Terapista");
			doc.text(leftAlign+45, inizioRow-5, "Data Appuntamento");
			doc.text(leftAlign+90, inizioRow-5, "Durata");
			doc.text(leftAlign+120, inizioRow-5, "Tariffa");
			doc.text(leftAlign+150, inizioRow-5, "Totale");
			doc.text(leftAlign+170, inizioRow-5, "Quota St.30%");
			doc.text(leftAlign+200, inizioRow-5, "Quota Ter.70%");
			row=0;
		}
		doc.setFont("times");
		doc.setFontSize(10);
		if(obj[i].stato==2){
			doc.setFontType("bolditalic");
			doc.setTextColor(255,0,0);
		}else{
			doc.setFontType("bold");
			doc.setTextColor(0,0,0);
		}
		var dateSt = new Date(obj[i].start);
		var dateEn = new Date(obj[i].end);
		var ora = dateEn-dateSt;
		var ora = ora/3600000;
		//1 = calcolo in percentuale, 0 quota Fissa
		if(obj[i].percentuale==="1"){
			var quota_user = parseFloat(obj[i].quota/100);
			var quota_studio = parseFloat(1 - quota_user);
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa)*ora)*quota_studio;
			totaleTe += (parseFloat(obj[i].tariffa)*ora)*quota_user;
		}else{
			totale += parseFloat(obj[i].tariffa)*ora;
			totaleSt += (parseFloat(obj[i].tariffa - obj[i].quota)*ora);
			totaleTe += (parseFloat(obj[i].quota)*ora);	
		}
		doc.text(leftAlign, inizioRow+row, obj[i].nomeCognome_t);
		doc.text(leftAlign+45, inizioRow+row, convertiData(obj[i].start,"anno",0)+" - "+convertiData(obj[i].start,"ora",0));
		doc.text(leftAlign+90, inizioRow+row, String(ora.toFixed(2)));
		doc.text(leftAlign+120, inizioRow+row, obj[i].tariffa);
		doc.text(leftAlign+150, inizioRow+row, formatComma((parseFloat(obj[i].tariffa)*ora).toFixed(2)));
		if(obj[i].percentuale==="1"){
			var quota_user = parseFloat(obj[i].quota/100);
			var quota_studio = parseFloat(1 - quota_user);
			doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_studio).toFixed(2)));
			doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa)*ora)*quota_user).toFixed(2)));
		}else{
			doc.text(leftAlign+170, inizioRow+row, formatComma(((parseFloat(obj[i].tariffa - obj[i].quota)*ora)).toFixed(2)));
			doc.text(leftAlign+200, inizioRow+row, formatComma(((parseFloat(obj[i].quota)*ora)).toFixed(2)));
		}
		
		row+=5;
		
	}
	
		doc.setLineWidth(1);
		doc.line(leftAlign, inizioRow+145, 280, inizioRow+145);
		doc.setFont("times");
		doc.setFontSize(12);
		doc.setFontType("italic");
		doc.setTextColor(0,0,0);
		doc.text(leftAlign, inizioRow+150, "");
		doc.text(leftAlign+45, inizioRow+150, "");
		doc.text(leftAlign+90, inizioRow+150, "");
		doc.text(leftAlign+120, inizioRow+150, "");
		doc.text(leftAlign+150, inizioRow+150, formatComma(totale.toFixed(2)));
		doc.text(leftAlign+170, inizioRow+150, formatComma(totaleSt.toFixed(2)));
		doc.text(leftAlign+200, inizioRow+150, formatComma(totaleTe.toFixed(2)));
	// Output as Data URI
	$.LoadingOverlay("hide");
	if(preview){
		doc.output('datauri');
		
	}else{
		doc.save('Report_Pazienti.pdf')
		
	}
	if(pagati && !preview){
		segnaAgendaPagati(obj);
	}
	
}

function segnaAgendaPagati(agenda){
	
	var value= "(";
	for(var i = 0; i < obj.length; i++){
		if(i<obj.length-1){
			value += obj[i].id+",";
		}else{
			value += obj[i].id+")";
		}
	}
	
	Query = "UPDATE agenda SET stato = 3 WHERE id IN "+value;
	

	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': Query
		  },
		  success: function (response) {
			
		  },
		  error: function () {
			alert("Errore nell'esecuzione della Query! \n"+Query);
		  }
	});

	
}


