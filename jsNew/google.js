 
      // Client ID and API key from the Developer Console
      var CLIENT_ID = client_id;//'978583980717-3v60hscd395ogkpf918v36qsi5clt799.apps.googleusercontent.com';  
	  
      var API_KEY = googleAPI;//'AIzaSyB3kJ9VZiRc-C55SsrkshBbQG4gzwD1E2M';//'AIzaSyBjsn28-x7iTyyfW6_YfGUPKEM5X95VbX8';

      // Array of API discovery doc URLs for APIs used by the quickstart
      var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
	  
	  

	  

      // Authorization scopes required by the API; multiple scopes can be
      // included, separated by spaces.
      var SCOPES = "https://www.googleapis.com/auth/calendar";
	  var id_Paziente_google;
	  var nomeCognome_google;
	  var logged = false;
	 
      /**
       *  On load, called to load the auth2 library and API client library.
       */
	   
	   var username = checkCookie();

	   var catId = username[5].substring(0,username[5].indexOf("-"));
	  
      function handleClientLoad() {
		  var windowLoc = $(location).attr('pathname');
		 
			gapi.load('client:auth2', initClient);
			//GOOGLEinitPazientiGrid();
			ritorna_nome_terapista();
			ListeTariffeGoogle();
			ListeStanzeGoogle();
			
			logged = true;
			
			
			
      }
	  
	  function handleClientLoadOUT() {
        gapi.load('client:auth2', initClient1);
      }

      /**
       *  Initializes the API client library and sets up sign-in state
       *  listeners.
       */
      function initClient() {
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(function () {
          // Listen for sign-in state changes.
          gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

          // Handle the initial sign-in state.
          updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
		  
		
		  
		});
      }


	  function controlla() {
        gapi.client.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          discoveryDocs: DISCOVERY_DOCS,
          scope: SCOPES
        }).then(function () {
			ritorna_nome_terapista();
			ListeTariffeGoogle();
			ListeStanzeGoogle();
			
		  // Handle the initial sign-in state.
         var stato = gapi.auth2.getAuthInstance().isSignedIn.get();
		  
		  
		  
		if (stato){
			window.parent.$('#button_sync').hide();
			  listUpcomingEvents();
			  
			  
		}
          // Handle the initial sign-in state.
          
        });
      }
	  
   function initClient1() {
		gapi.client.init({
		  apiKey: API_KEY,
		  clientId: CLIENT_ID,
		  discoveryDocs: DISCOVERY_DOCS,
		  scope: SCOPES
		}).then(function () {
		  // Listen for sign-in state changes.
		  //gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

		  // Handle the initial sign-in state.
		  logged = false;
		  GoogleSignout();
		  
		  
		  window.location="php/logout.php";
		  
		});
}

function disableButton(){
	
	if(basic==1){
		handleClientLoad();
	} else {
	 $('#contattaSupporto').modal();
	}
}
var connect = false;
 function controllaStatus() {
	  gapi.load('client:auth2', controlla);
	  // Listen for sign-in state changes.
	}

      /**
       *  Called when the signed in status changes, to update the UI
       *  appropriately. After a sign-in, the API is called.
       */
      function updateSigninStatus(isSignedIn) {
		var windowLoc = $(location).attr('pathname');
		console.log(windowLoc);
		 if (isSignedIn) {
			listUpcomingEvents();
				window.parent.$('#button_sync').hide();
		} else {
				
				if(connect==true) {
					
				} else {
					
				gapi.auth2.getAuthInstance().signIn();
				
				connect = true;
				
				}
		} 
		 
		}

      /**
       *  Sign in the user upon button click.
       */
      function handleAuthClick(event) {
        gapi.auth2.getAuthInstance().signIn();
      }

      /**
       *  Sign out the user upon button click.
       */
      function GoogleSignout(event) {
		gapi.auth2.getAuthInstance().disconnect();
		connect = false;
      }

      /**
       * Append a pre element to the body containing the given message
       * as its text node. Used to display the results of the API call.
       *
       * @param {string} message Text to be placed in pre element.
       */
   /*    function appendPre(message) {
		var pre = document.getElementById('content');
        var textContent = document.createTextNode(message + '\n');
        pre.appendChild(textContent);
      } */

      /**
       * Print the summary and start datetime/date of the next ten events in
       * the authorized user's calendar. If no events are found an
       * appropriate message is printed.
       */
lista_inserita=false;
var evento_list = [];
      function listUpcomingEvents() {
        gapi.client.calendar.events.list({
          'calendarId': 'primary',
          'timeMin': (new Date()).toISOString(),
          'showDeleted': false,
          'singleEvents': true,
          'maxResults': 1000,
          'orderBy': 'startTime'
        }).then(function(response) {
			
          var events = response.result.items;
		  
		  var windowLoc = $(location).attr('pathname');
		  
		  //if (windowLoc.indexOf('calendar.html')>-1) {
		  
		  var evento_google = {};
				  
				  for (i=0;i<events.length;i++){
					  
						evento_google[i] = {"title":events[i].summary,
											"start":events[i].start.dateTime,
											"end":events[i].end.dateTime,
											"nota":events[i].description,
											"email":events[i].organizer.email,
											"google":1,
											"id": events[i].id
										   };
						
						evento_list.push(evento_google[i]);
				  }
				  
				  //console.log(evento_list);
				  
				  if (lista_inserita==false){
						$('#calendar').fullCalendar( 'addEventSource', evento_list);
						$('#calendar').fullCalendar( 'rerenderEvents' );
						lista_inserita=true;
				  } 
		 // } 
		
		});
      }
	  
function creaEvento(){
// Refer to the JavaScript quickstart on how to setup the environment:
// https://developers.google.com/google-apps/calendar/quickstart/js
// Change the scope to 'https://www.googleapis.com/auth/calendar' and delete any
// stored credentials
var date = new Date();
var offset = date.getTimezoneOffset().toString();;
var start = calcolaDataDurata(datiAppuntamento.start.format("dddd DD-MM-YYYY HH:mm"),offset,2).replace(" ","T")+"-00:00";//String(datiAppuntamento.start.format("YYYY-MM-DD HH:mm"));
var end = calcolaDataDurata(datiAppuntamento.end.format("dddd DD-MM-YYYY HH:mm"),offset,2).replace(" ","T")+"-00:00";
var event = {
  'summary': datiAppuntamento.title,
  //'location': '800 Howard St., San Francisco, CA 94103',
  'description': datiAppuntamento.nota,
  'start': {
    'dateTime': start,
    'timeZone': 'UTC'
  },
  'end': {
    'dateTime': end,
    'timeZone': 'UTC'
  },

  'reminders': {
    'useDefault': false,
    'overrides': [
      {'method': 'email', 'minutes': 24 * 60},
      {'method': 'popup', 'minutes': 10}
    ]
  }
}; 

 var request = gapi.client.calendar.events.insert({
  'calendarId': 'primary',
  'resource': event
});

var stato = gapi.auth2.getAuthInstance().isSignedIn.get();

if(stato) {
		request.execute(function(event) {
			
			
				  //appendPre('Event created: ' + event.htmlLink);
				  window.parent.document.getElementById('myBtn').click(); 
				  $('#GoogleSincronizzato').modal();
				  location.reload();
			
		}); 
} else {
	var windowLoc = $(location).attr('pathname');
	launchModalGoogle();
}
}

function launchModalGoogle(){
	window.parent.document.getElementById('myBtn').click(); 
	$('#RichiestaLoginGoogle').modal();
}

function EliminaEventoGoogle(){
// Refer to the JavaScript quickstart on how to setup the environment:
// https://developers.google.com/google-apps/calendar/quickstart/js
// Change the scope to 'https://www.googleapis.com/auth/calendar' and delete any
// stored credentials.

var eventId = datiAppuntamento.id;


 var request = gapi.client.calendar.events.delete({
  'calendarId': 'primary',
  'eventId': eventId
});


request.execute(function(event) {
  //appendPre('Event created: ' + event.htmlLink);
  window.parent.document.getElementById('myBtn').click(); 
  $('#GoogleSincronizzato').modal();
  location.reload();
}); 
}





function TrovaStanzaTariffaGoogle() {
	 
	 var nome = return_nome_terapista;
	 
	 for (i=0;i<terapisti_google.length;i++){
		 if (terapisti_google[i] == nome) {
			 stanza_terapista_google  = stanza_google[i];
			 tariffa_terapista_google = tariffa_google[i];
			 valoreIdStanza_google = id_stanza_google[i];
			 valoreIdTariffa_google = id_tariffa_google[i];
			 //valoreIdTerapista_google = id_terapista_google[i];
		 }
	 }
	
		$('#TerapistaAppuntamento_google').val(nome);
		$('#StanzaAppuntamento_mod2_google').val(stanza_terapista_google);
		$('#TariffaAppuntamento_mod2_google').val(tariffa_terapista_google+" €");
	 

	 return stanza_terapista
 }

var terapisti_google = [];
var stanza_google = [];
var tariffa_google = [];
//var id_terapista_google = [];
var id_stanza_google = [];
var id_tariffa_google = []; 
 
function QueryTerapistiGoogle(){
	
	var query_cerca = 'SELECT st.stanza, ta.tariffa, st.ID as idstanza, te.nomeCognome_t, ta.ID as idtariffa FROM Terapisti as te JOIN Tariffa as ta ON ta.ID = te.idTariffa JOIN Stanza as st ON st.ID = te.idStanza where te.ID = ' + username[1] + '';
	
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_cerca
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			
			terapisti_google = [];
			stanza_google = [];
			tariffa_google = [];
			id_tariffa = [];
			//id_terapista_google = [];
			id_stanza_google = [];
	

			for (i=0;i<obj.length;i++){
				
				terapisti_google[i] = obj[i].nomeCognome_t;
				stanza_google[i] = obj[i].stanza;
				tariffa_google[i] = obj[i].tariffa.replace(".",",");
				id_stanza_google[i] = obj[i].idstanza;
				id_tariffa_google[i] = obj[i].idtariffa;
				//id_terapista_google[i] = obj[i].idTerapista;
			}
			
			TrovaStanzaTariffaGoogle()
			
		 },
		  error: function () {
			alert("error");
		  }
	});
}



var return_nome_terapista;

function ritorna_nome_terapista() {
	
	var index = username[1];
	
	if (index!=undefined) {
		
		var query_return_terapista = 'SELECT * FROM `Terapisti` WHERE ID ='+index+'';
		
	}
	
	
		$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_return_terapista
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			
			return_nome_terapista = obj[0].nomeCognome_t;
			
			if (return_nome_terapista!="Administrator") {
			QueryTerapistiGoogle(return_nome_terapista);
			}
		},
		  error: function () {
			alert("error");
		  }
		  
		  
	});
	
}  


var tariffe_google=[];
var id_tariffe_google=[];
function ListeTariffeGoogle(){
	
	var query_cercaTariffeGoogle = 'SELECT * FROM `Tariffa` WHERE custom = 0';
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_cercaTariffeGoogle
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			
			
			for (i=0;i<obj.length;i++){
				
				tariffe_google[i] = obj[i].tariffa;
				id_tariffe_google[i] = obj[i].ID;
				
			}
			
			creaListeTariffeGoogle();
		},
		  error: function () {
			alert("error");
		  }
	});
}

lista_tariffe_google = false;
function creaListeTariffeGoogle(){
	
			if (lista_tariffe_google==false) {
			
			var select = document.getElementById("TariffaAppuntamento_mod_google");
			
			for (t=0;t<tariffe_google.length;t++){
				var options = tariffe_google[t].replace(".",",");
				var opt = options;
				var el = document.createElement("option");
				el.textContent = opt;
				el.value = opt;
				select.appendChild(el);
			}
			lista_tariffe_google = true;
			
		} else if (lista_tariffe_google=true) {
			
		var select = document.getElementById("TariffaAppuntamento_mod_google");
		
		var i;
		for(i = select.length - 1 ; i >= 0 ; i--)
		{
			select.remove(i);
		}
		
		for (t=0;t<tariffe_google.length;t++){
			var options = tariffe_google[t].replace(".",",");
			var opt = options;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = opt;
			select.appendChild(el);
		}
		}
		
	
}


var stanze_google=[];
var id_stanze_google=[];
function ListeStanzeGoogle(){
	
	var query_cercaStanze = 'SELECT * FROM `Stanza`';
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_cercaStanze
		  },
		  success: function (response) {
			obj = JSON.parse(response);
			
			for (i=0;i<obj.length;i++){
				
				stanze_google[i] = obj[i].stanza;
				id_stanze_google[i] = obj[i].ID;
				
			}
			
			creaListeStanzeGoogle();
		},
		  error: function () {
			alert("error");
		  }
	});
}

lista_stanze_google = false;
function creaListeStanzeGoogle(){
	
			if (lista_stanze_google==false) {
			
			var select = document.getElementById("StanzaAppuntamento_mod_google");
			
			for (t=0;t<stanze_google.length;t++){
				var options = stanze_google[t];
				var opt = options;
				var el = document.createElement("option");
				el.textContent = opt;
				el.value = opt;
				select.appendChild(el);
			}
			lista_stanze = true;
			
		} else if (lista_stanze_google=true) {
			
		var select = document.getElementById("StanzaAppuntamento_mod_google");
		
		var i;
		for(i = select.length - 1 ; i >= 0 ; i--)
		{
			select.remove(i);
		}
		
		for (t=0;t<stanze_google.length;t++){
			var options = stanze_google[t];
			var opt = options;
			var el = document.createElement("option");
			el.textContent = opt;
			el.value = opt;
			select.appendChild(el);
		}
		}
		
	
}

function GOOGLESalvaEvento(){
	
	var now_google = new Date().getTime();
	var titolo_google = $('#TitoloAppuntamento_google').val();
	var nome_google = $('#NomeAppuntamento_google').val();
	var data_google = convertiData(datiAppuntamento.start,"anno2",2);
	var data2_google = convertiData($('#data_google').val(),"anno2",1);
	if (data2_google=="" | data2_google=="NaN-undefined-NaN") {
		data2_google = data_google;
	}
	
	var oraInizio_google = convertiData(datiAppuntamento.start,"ora");
	var oraInizio2_google = $('#oraInizio_google').val();
	if (oraInizio2_google=="") {
		oraInizio2_google = oraInizio_google;
	}
	
	var oraFine_google = convertiData(datiAppuntamento.end,"ora");
	var oraFine2_google = $('#oraFine_google').val();
	if (oraFine2_google=="") {
		oraFine2_google = oraFine_google;
	}
	
	var dataFinaleInizio_google = data2_google + " " + oraInizio2_google + ":00"
	var dataFinaleFine_google = data2_google + " " + oraFine2_google + ":00"
	
	//verifico il nome del terapista ed il relativo id
	var terapista_google = $('#TerapistaAppuntamento_google').val();
	var id_terapista_google = username[1];
	
//verifico il nome della stanza ed il relativo id
	var stanza_google = $('#StanzaAppuntamento_mod2_google').val();
	var stanza1_google = $('#StanzaAppuntamento_mod_google').val();
		if (stanza1_google==null) {
			stanza1_google = stanza_google;
		}
		
		for (i=0;i<stanze_google.length;i++) {
			if (stanze_google[i]==stanza1_google){
				var idstanza_google = id_stanze_google[i];
				//console.log(id_stanza_mod);
				break
			}
		}
	
		
		var tariffeGoogle = $('#TariffaAppuntamento_mod2_google').val().substring(0,$('#TariffaAppuntamento_mod2_google').val().indexOf(' ')).replace(",",".");
		
		var tarGoogle = $('#TariffaAppuntamento_mod_google').val();
		if (tarGoogle!=null){
		var tariffeGoogle1 = tarGoogle[0].replace(",",".");
		}

				if (tariffeGoogle1==undefined) {
					tariffeGoogle1 = tariffeGoogle;
				}
				
				//var tariffesad = tariffeGoogle1.replace(",",".");
				for (i=0;i<tariffe_google.length;i++) {
					if (tariffe_google[i]==tariffeGoogle1){
						var idtariffa_google = id_tariffe_google[i];
						//console.log(id_stanza_mod);
						break
					}
				}
	
	
	
	var note_google = $('#NoteAppuntamento_google').val();
	
	if (nome_google==""|titolo_google==""|data2_google==""|terapista_google==" "|tariffeGoogle1==""|stanza1_google==""){
		window.parent.document.getElementById('myBtn').click(); 
		$('#CompilaTuttiCampi').modal();
	} else {
		
	valori_google = "("+id_Paziente_google+",'"+titolo_google+"','"+note_google+"','"+dataFinaleInizio_google+"','"+dataFinaleFine_google+"',"+0+","+username[1]+","+idtariffa_google+","+idstanza_google+",'"+"Nessuna"+"',"+0+","+now_google+","+catId+")";

	
	var query_completa_google = "INSERT INTO `agenda`(`idPaziente`, `title`, `nota`, `start`, `end`, `eventoCancellato`, `idTerapista`, `idTariffa`, `idStanza`, `ricorrenza`, `stato`, `now`, `IdCat`) VALUES " + valori_google;
		
	//console.log(query_completa_google);
		$.ajax({
		
		  type: 'post',
		  url: '../php/mysql/Query_Insert_Delete.php',
		  data: { 
			'query': query_completa_google
		  },
		  success: function (response) {
			
			$('#sincronizzaGoogle').modal('toggle');
			location.reload();
			//alert(response);
			
			},
		  
		  error: function () {
			alert("error");
		  }
	}); 
	
}
}


function GOOGLEinitPazientiGrid(w){
	
	var query_cercaPaziente_google = 'SELECT * FROM joinPazienteTariffaTerapista AS j JOIN Terapisti AS t ON t.ID = j.idTerapista JOIN  paziente AS p on p.idPaziente = j.idPaziente where t.ID ="'+username[1]+'"';
	
	$.ajax({
		  type: 'post',
		  url: '../php/mysql/Query_Select.php',
		  data: { 
			'query': query_cercaPaziente_google
		  },
		  success: function (response) {
			//alert(response);
			obj = JSON.parse(response);
			//console.log(obj);
			showGridGoogle(obj,w);
		  },
		  error: function () {
			alert("error");
		  }
	});	
}

var immagine = false;
function imageRequest(){
	
	if (immagine==false) {
		if(username[2]!="admin"){
		$.ajax({
		  type: 'get',
		  url: 'https://picasaweb.google.com/data/entry/api/user/'+username[2]+'?alt=json',
		  success: function (response) {
			//window.parent.$('#logoutButton').html("");
			var data_img = response.entry.gphoto$thumbnail.$t;
			var testo = window.parent.$('#logoutButton').html();
			var testo2 = testo.substring(testo.lastIndexOf(" "),testo.length);
			window.parent.$('#logoutButton').html('<img id="round" src="'+data_img+'" width="25" />'+testo2);
		  },
		  error: function () {
			var testo = window.parent.$('#logoutButton').html();
			var testo2 = testo.substring(testo.lastIndexOf(" "),testo.length);
			window.parent.$('#logoutButton').html('<img id="round" src="agenda/images/avatar.png" width="25" />'+testo2);
		  //alert("error");
		  }
	});
		} else {
			//se l'utente è admin, l'immagine del profilo è quella standard
			var testo = window.parent.$('#logoutButton').html();
			var testo2 = testo.substring(testo.lastIndexOf(">")+1,testo.length);
			window.parent.$('#logoutButton').html('<img id="round" src="agenda/images/avatar.png" width="25" />'+testo2);
			
		}
	immagine = true;
	} else 
	{}
}




function launchModal() {
	
	document.getElementById('myBtn').click(); 
	$('#Logout').modal();
}


function showGridGoogle(obj,w) {
	
	$grid = $("#jqGrid_google");
	$grid.jqGrid('GridUnload');
	
    $grid.jqGrid({
        data: obj,
        datatype: "local",
        colModel: [
            { name: "nomeCognome", label: "Cognome Nome:", width: 445 }
            
        ],
		//autowidth: true,
		width: w,
		shrinkToFit: true,
        pager: "#packagePager1",
        rowNum: 10,
        rowList: [1, 2, 10],
        viewrecords: true,
		height: "auto",
        sortname: "nomeCognome",
		caption: "cerca",
        autoencode: true,
        gridview: true,
        ignoreCase: true,
        onSelectRow: function (rowid) {
            var rowData = $(this).jqGrid("getLocalRow", rowid), str = "", p;
			id_Paziente_google = rowData.idPaziente;
			nomeCognome_google = rowData.nomeCognome;
		}
    });
    $grid.jqGrid("navGrid", "#packagePager1",
        { add: false, edit: false, del: false }, {}, {}, {},
        { multipleSearch: true, multipleGroup: true });
    $grid.jqGrid("filterToolbar", { defaultSearch: "cn", stringResult: true });
}

